'use strict'

gulp = require('gulp')
source = require('vinyl-source-stream')
buffer = require('vinyl-buffer')
size = require('gulp-size')
gulpif = require('gulp-if')
concat = require('gulp-concat')
replace = require('gulp-replace')
classPrefix = require('gulp-class-prefix')
sass = require('gulp-sass')
plumber = require('gulp-plumber')
autoprefixer = require('gulp-autoprefixer')
csso = require('gulp-csso')
uglify = require('gulp-terser')
browserSync = require('browser-sync').create()
browserify = require('browserify')
coffeeify = require('coffeeify')
css = require('browserify-css')
zip = require('gulp-zip')
babel = require('gulp-babel')
del = require('del')
slim = require("gulp-slim")
sourcemaps = require('gulp-sourcemaps')
stripCssComments = require('gulp-strip-css-comments')
gulpCopy = require('gulp-copy')
CLASS_PREFIX = require('./app/scripts/chart/prefix')
flatten = require('gulp-flatten')


IS_PRODUCTION = false
IS_UGLIFY_ENABLED = false

config =
  chartStyle: 'app/styles/chart.css'
  appStyle: 'app/styles/app.css'
  stylesWatch: ['app/styles/**/*.sass', 'app/styles/**/*.css'],
  html: 'app/index.slim'
  scripts: ['app/scripts/app.js', 'app/scripts/chart/chart.js']
  scriptsWatch: ['app/scripts/**/*.coffee', 'app/scripts/**/*.js']
  jsonData: './app/data/**/*.json'

  favicon: 'app/favicon.ico'
  images: 'app/images/**/*.*'
  readme: 'README.txt'

  to: './dist'


gulp.task 'html', ->
  gulp.src config.html
    .pipe slim({ pretty: true })
    .pipe gulp.dest(config.to)

gulp.task 'chartStyle:prepare', ->
  gulp.src config.chartStyle
    .pipe plumber()
    .pipe sass()
    .pipe autoprefixer(browsers: ['last 1 version'])
    .pipe stripCssComments()
    .pipe csso()
    .pipe classPrefix( CLASS_PREFIX )
    .pipe gulp.dest("#{config.to}/styles/")

gulp.task 'appStyle:prepare', ->
  gulp.src config.appStyle
    .pipe plumber()
    .pipe sass()
    .pipe autoprefixer(browsers: ['last 1 version'])
    .pipe stripCssComments()
    .pipe csso()
    .pipe gulp.dest("#{config.to}/styles/")

gulp.task 'copy:appjs', ->
  gulp.src config.scripts[0]
    .pipe(flatten())
    .pipe gulp.dest(config.to)

gulp.task 'copy:json', ->
  gulp.src config.jsonData
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:favicon', ->
  gulp.src config.favicon
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:images', ->
  gulp.src config.images
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:readme', ->
  gulp.src config.readme
    .pipe gulpCopy(config.to, {})


babelProps =
  presets: [
    "@babel/preset-env"
  ],
  plugins: [
    "@babel/plugin-transform-object-assign",
    "@babel/plugin-transform-classes",
    "@babel/plugin-transform-template-literals"
    [ "@babel/plugin-proposal-decorators", { "legacy": true } ],
    ["@babel/plugin-proposal-class-properties", { "loose": true }]
  ]


bundle = (bundler, fileName) ->
  bundlerStream = bundler
    .bundle()
    .pipe plumber()
    .pipe source(fileName)
    .pipe buffer()
    .pipe gulpif(IS_UGLIFY_ENABLED, uglify())
    .pipe concat(fileName)
    .pipe gulp.dest(config.to)
    .pipe size(title: "total", gzip: true)
    # .pipe replace('process.env.NODE_ENV', "\"#{process.env.NODE_ENV}\"")
  return bundlerStream

gulp.task "browserify", (done) ->
  if IS_PRODUCTION
    babelProps.plugins.push('transform-remove-console')
    babelProps.comments = false

  _file = config.scripts[1]
  bundler = browserify({
    entries: [_file]
    dest: config.to
    # outputName: 'app.js'
    extensions: ['.js', '.coffee']
  })
  .transform("coffeeify", {
    transpile: Object.assign({}, babelProps)
  })
  .transform('babelify', Object.assign({}, babelProps))
  .transform(css, autoInject: true)

  bundler.on('update', () -> bundle(bundler))
  bundlerStream = bundle(bundler, _file.split('/').pop())


gulp.task "reload", (done) ->
  browserSync.reload()
  done()

gulp.task "serve", () ->
  browserSync.init(server: config.to)
  gulp.watch config.stylesWatch, gulp.series(gulp.parallel('chartStyle:prepare', 'appStyle:prepare', 'copy:appjs', 'browserify'), 'reload')
  gulp.watch config.scriptsWatch, gulp.series('copy:appjs', 'browserify', 'reload')
  gulp.watch config.html, gulp.series('html', 'reload')
  gulp.watch config.jsonData, gulp.series('copy:json', 'reload')
  gulp.watch config.favicon, gulp.series('copy:favicon', 'reload')

gulp.task 'set_dev_env', (cb) ->
  process.env.NODE_ENV = 'development'
  cb()

gulp.task 'set_prod_env', (cb) ->
  process.env.NODE_ENV = 'production'
  IS_PRODUCTION = true
  cb()

gulp.task 'set_deploy_env', (cb) ->
  IS_UGLIFY_ENABLED = true
  cb()


gulp.task 'clean', del.bind(null, config.to)
gulp.task "once", gulp.series('clean', gulp.parallel('html', 'chartStyle:prepare', 'appStyle:prepare', 'copy:json', 'copy:favicon', 'copy:images', 'copy:appjs', 'browserify'))
gulp.task "dev", gulp.series('set_dev_env', 'once', 'serve')
gulp.task "build", gulp.series('set_prod_env', 'once')
gulp.task "buildForDeploy", gulp.series('set_prod_env', 'set_deploy_env', 'once')
