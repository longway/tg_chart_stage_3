import renderStackedZoom from './renderStackedZoom'


export default function renderStacked(
  ctx,
  chart,
  data,
  width,
  paddingHor,
  isZoomMorphing,
  preview,
  selectI,
  zoomMinI,
  zoomMaxI,
  zoomAnimation
) {
  var step = Math.floor((chart.maxI - chart.minI) / (width - paddingHor * 2))
  if (step < 1) step = 1

  var columnWidth = Math.floor(data.intervalX * chart.scaleX) + 1
  var offsetX = chart.offsetX

  var isPreview = chart === preview

  if( zoomAnimation.value < 1 ) {
    for (var i = chart.minI; i < chart.maxI; i += step) {
      if (isZoomMorphing && i >= zoomMinI && i < zoomMaxI) continue
      var x = Math.floor(data.xColumn.data[i] * chart.scaleX + offsetX)

      var from = chart.offsetY
      const isNotFaded = isPreview || selectI === -1 || i === selectI

      for (var j = 0; j < data.columns.length; j++) {
        var yColumn = data.columns[j]
        if (yColumn.alpha.value === 0) continue

        var y = yColumn.data[i] * chart.scaleY
        y *= yColumn.alpha.value
        y = Math.floor(y)

        ctx.fillStyle = isNotFaded ? data.colors[yColumn.name] : yColumn.fadedColor
        ctx.fillRect(x, from, columnWidth, y)

        from += y
      }
    }
  }

  if (isZoomMorphing) {
    renderStackedZoom(ctx, chart.zoomStartI, chart.zoomEndI, chart, data, zoomAnimation, isPreview, selectI)
  }
}
