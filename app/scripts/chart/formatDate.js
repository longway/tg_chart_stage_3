import formatNumberToMinTwoDigits from './formatNumberToMinTwoDigits'


const STYLES = {
  x: 1,
  popup: 2,
  title: 3,
  time: 4,
}
const MONTH_NAMES_SHORT = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const MONTH_NAMES_LONG = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const DAY_NAMES = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']


export default function formatDate(time, style, isZoomed, fullMonth = false) {
  if (style === STYLES.popup && isZoomed) { style = STYLES.time }
  if (style === STYLES.x && isZoomed) { style = STYLES.time }

  var date = new Date(time)
  const _monthNum = date.getMonth()
  var s = `${ date.getDate() } ${ fullMonth ? MONTH_NAMES_LONG[_monthNum] : MONTH_NAMES_SHORT[_monthNum] }`

  if (style === STYLES.time) {
    return `${ formatNumberToMinTwoDigits(date.getHours()) }:${ formatNumberToMinTwoDigits(date.getMinutes()) }`

  }

  if (style === STYLES.title) {
    return `${ s } ${ date.getFullYear() }`
  }

  if (style === STYLES.popup) {
    return `${ DAY_NAMES[date.getDay()] }, ${ s } ${ date.getFullYear() }`
  }

  return s
}

export const formatDateStyles = STYLES
