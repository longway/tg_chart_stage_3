import renderTextsScaledY from './renderTextsScaledY'
import { CHART_KINDS } from './CHART_KINDS'
import formatNumber from './formatNumber'


export default function renderTextsY(
  ctx,
  textY,
  chartType,
  textCountY,
  toScreenY,
  minY,
  zoomOffsetY,
  width,
  paddingHor,
  textYMargin,
  data,
  textXWidth,
) {
  if (textY.alpha.value === 0) { return }

  ctx.globalAlpha = textY.alpha.value

  if (chartType === CHART_KINDS.yScaled) {
    renderTextsScaledY(ctx, 0, textY, paddingHor, data, textCountY, toScreenY, minY, zoomOffsetY, textYMargin)
    renderTextsScaledY(ctx, 1, textY, width - paddingHor - textXWidth, data, textCountY, toScreenY, minY, zoomOffsetY, textYMargin)
  } else {
    for (var i = 0; i <= textCountY; i++) {
      var y = toScreenY(minY.value + textY.delta * i)
      var value = minY.toValue + textY.delta * i - zoomOffsetY
      ctx.fillText(formatNumber(value, true), paddingHor, y + textYMargin)
    }
  }
}
