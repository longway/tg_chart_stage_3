import createElement from './createElement'
const CLASS_PREFIX = require('./prefix')


const CHART_ANIMATED_CLASS_NAME = CLASS_PREFIX + 'chart__animated'

export default function createAnimatedText(container, align = 'right', childs = null) {
  if( !childs ) {
    childs = [createElement(container, 'span'), createElement(container, 'span')]
  }

  var alignClass = CLASS_PREFIX + 'align-' + align
  childs[0].classList.add(CHART_ANIMATED_CLASS_NAME, alignClass)
  childs[1].classList.add(CHART_ANIMATED_CLASS_NAME, alignClass)

  var textObj = {
    container,
    childs,
    current: 1,
    startTime: 0,
    texts: ['', ''],
    clearTexts: function clearTexts() {
      this.texts[0] = ''
      this.texts[1] = ''
    },
    clearVisibleClass: function clearVisibleClass(childId) {
      this.childs[childId].classList.remove(CLASS_PREFIX + 'visible')
    },
    clearHiddenClass: function clearHiddenClass(childId) {
      this.childs[this.current].classList.remove(CLASS_PREFIX + 'hidden')
    },
    clearAnimatedClasses: function(childId) {
      this.childs[childId].classList.remove(CLASS_PREFIX + 'animated-top-left')
      this.childs[childId].classList.remove(CLASS_PREFIX + 'animated-top-right')
      this.childs[childId].classList.remove(CLASS_PREFIX + 'animated-bottom-left')
      this.childs[childId].classList.remove(CLASS_PREFIX + 'animated-bottom-right')
    },
    setText: function setText(text, time, force = false) {
      if( text === this.texts[this.current] && !force ) { return }

      if (
        this.texts[this.current].length > 0 &&
        time - this.startTime > 300
      ) {
        var notCurrent = Math.abs(this.current - 1)
        this.startTime = time
        // NOTE: POSSIBLE FIX RECALC STYLES USED ".textContent" INSTEAD OF ".innerText"
        this.childs[notCurrent].textContent = text
        this.texts[notCurrent] = text
        this.toggle()
        return
      }

      this.childs[this.current].textContent = text
      this.texts[this.current] = text
    },
    toggle: function toggle() {
      var notCurrent = this.current
      this.current = Math.abs(this.current - 1)

      this.clearVisibleClass(notCurrent)
      this.clearAnimatedClasses(notCurrent)
      this.childs[notCurrent].classList.add(CLASS_PREFIX + 'hidden', CLASS_PREFIX + 'animated-top-' + align)

      this.clearHiddenClass(this.current)
      this.clearAnimatedClasses(this.current)
      this.childs[this.current].classList.add(CLASS_PREFIX + 'visible', CLASS_PREFIX + 'animated-bottom-' + align)
    },
    toggleReverse: function toggleReverse() {
      var notCurrent = this.current
      this.current = Math.abs(this.current - 1)

      this.clearVisibleClass(notCurrent)
      this.clearAnimatedClasses(notCurrent)
      this.childs[notCurrent].classList.add(CLASS_PREFIX + 'hidden', CLASS_PREFIX + 'animated-bottom-' + align)

      this.clearHiddenClass(this.current)
      this.clearAnimatedClasses(this.current)
      this.childs[this.current].classList.add(CLASS_PREFIX + 'visible', CLASS_PREFIX + 'animated-top-' + align)
    }
  }

  textObj.toggle()

  return textObj
}
