import renderPath from './renderPath'
import { CHART_KINDS } from './CHART_KINDS'
import { PIXEL_RATIO } from './constants'
var LINE_WIDTH = 2 * PIXEL_RATIO


export default function renderMainPath(ctx, chart, data, globalAlpha, width, paddingHor, zoom) {
  if( data.chartType === CHART_KINDS.line || data.chartType === CHART_KINDS.yScaled ) {
    for (var j = 0; j < data.columns.length; j++) {
      var yColumn = data.columns[j]

      if (yColumn.alpha.value === 0) continue

      ctx.globalAlpha = yColumn.alpha.value * globalAlpha

      ctx.lineWidth = LINE_WIDTH

      var columnScaleY = chart.scaleY * yColumn.scaleY

      renderPath(ctx, width, paddingHor, zoom, yColumn, chart.scaleX, columnScaleY, chart.offsetX, chart.offsetY, chart, data)
    }
  }
}
