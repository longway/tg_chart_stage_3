export default function removeEventListener(element, event, listener) {
  element.removeEventListener(event, listener)
}
