export default function playAnimation(anim, toValue, time) {
  anim.fromValue = anim.value
  anim.toValue = toValue
  anim.progress = 0
  anim.startTime = time
}
