import formatDate, { formatDateStyles } from './formatDate'
import { HOUR } from './timeConstants'


export default function renderTextsX(
  ctx,
  textX,
  main,
  data,
  textXWidth,
  textCountX,
  textXMargin,
  isZoomed,
) {
  if (textX.alpha.value === 0) { return }

  ctx.globalAlpha = textX.alpha.value

  var delta = textX.delta
  var endI = Math.ceil((main.maxX.value - data.xColumn.min) / HOUR / delta) * delta
  var startI = endI - delta * (textCountX + 1)

  for (var i = endI - 1; i >= startI; i -= delta) {
    var value = data.xColumn.min + (i - 1) * HOUR
    var x = main.toScreenX(value)
    var offsetX = -textXWidth / 2
    ctx.fillText(formatDate(value, formatDateStyles.x, isZoomed), x + offsetX, main.height + textXMargin)
  }
}
