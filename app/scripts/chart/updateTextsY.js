import playAnimation from './playAnimation'
import { CHART_KINDS } from './CHART_KINDS'


export default function updateTextsY(chartType, oldTextY, newTextY, time, minY, maxY, textCountY) {
  if (chartType === CHART_KINDS.percentage) {

    oldTextY.alpha.value = oldTextY.alpha.toValue = 0
    newTextY.alpha.value = newTextY.alpha.toValue = 1
    newTextY.delta = 25

  } else {

    oldTextY.delta = newTextY.delta
    oldTextY.alpha.value = newTextY.alpha.value
    playAnimation(oldTextY.alpha, 0, time)

    newTextY.delta = Math.floor((maxY.toValue - minY.toValue) / textCountY)
    newTextY.alpha.value = 1 - oldTextY.alpha.value
    playAnimation(newTextY.alpha, 1, time)

  }
}
