import toDoubleHexNumber from './toDoubleHexNumber'


export default function adjustBrightness(color) {
  var subtract = 20

  var R = parseInt(color.substring(1, 3), 16)
  var G = parseInt(color.substring(3, 5), 16)
  var B = parseInt(color.substring(5, 7), 16)

  R -= subtract
  G -= subtract
  B -= subtract

  if (R < 0) R = 0
  if (G < 0) G = 0
  if (B < 0) B = 0

  return '#' + toDoubleHexNumber(R.toString(16)) + toDoubleHexNumber(G.toString(16)) + toDoubleHexNumber(B.toString(16))
}
