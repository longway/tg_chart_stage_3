export default function addEventListener(element, event, listener) {
  element.addEventListener(event, listener, false)
}
