const chartKinds = {
  line: 1,
  yScaled: 2,
  stacked: 3,
  bar: 4,
  percentage: 5,
  pie: 6,
}

const names = {}
names[chartKinds.line] = 'Line'
names[chartKinds.yScaled] = 'Y-scaled'
names[chartKinds.stacked] = 'Stacked'
names[chartKinds.percentage] = 'Percentage'
names[chartKinds.pie] = 'Pie'
names[chartKinds.bar] = 'Bar'


export const CHART_KINDS = {
  ...chartKinds,
  names,
}
