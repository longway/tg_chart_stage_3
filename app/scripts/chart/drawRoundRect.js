export default function drawRoundRect(ctx, x, y, w, h, leftTopRadius, rightTopRadius, leftBottomRadius, rightBottomRadius) {
  ctx.moveTo(x + leftTopRadius, y)
  ctx.lineTo(x + w - rightTopRadius, y)
  ctx.quadraticCurveTo(x + w, y, x + w, y + rightTopRadius)
  ctx.lineTo(x + w, y + h - rightBottomRadius)
  ctx.quadraticCurveTo(x + w, y + h, x + w - rightBottomRadius, y + h)
  ctx.lineTo(x + leftBottomRadius, y + h)
  ctx.quadraticCurveTo(x, y + h, x, y + h - leftBottomRadius)
  ctx.lineTo(x, y + leftTopRadius)
  ctx.quadraticCurveTo(x, y, x + leftTopRadius, y)
}
