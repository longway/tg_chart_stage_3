function renderPathZoom(ctx, yColumn, minI, maxI, scaleX, scaleY, offsetX, offsetY, data, zoomAnimation) {
  for (var i = minI; i < maxI; i++) {
    var x = data.xColumn.toZoomData[i]
    x = x * scaleX + offsetX

    var y = yColumn.toZoomData[i] * zoomAnimation.value + yColumn.fromZoomData[i] * (1 - zoomAnimation.value)
    y = y * scaleY + offsetY

    ctx.lineTo(x, y)
  }
}

export default function renderPath(ctx, width, paddingHor, zoom, yColumn, scaleX, scaleY, offsetX, offsetY, chart, data) {
  ctx.strokeStyle = data.colors[yColumn.name]

  ctx.beginPath()
  ctx.lineJoin = 'bevel'
  ctx.lineCap = 'butt'

  var firstX = data.xColumn.data[chart.minI]
  var firstY = yColumn.data[chart.minI]
  // TODO
  // if( zoomAnimation.value < 1 || data.chartType === CHART_KINDS.bar )
    ctx.moveTo(firstX * scaleX + offsetX, firstY * scaleY + offsetY)

  var step = Math.floor((chart.maxI - chart.minI) / (width - paddingHor * 2))
  if (step < 1) step = 1

  var zoomRendered = false

  for (var i = chart.minI + 1; i < chart.maxI; i += step) {
    if (zoom.isMorphing && !zoomRendered && i >= zoom.minI) {
      renderPathZoom(ctx, yColumn, chart.zoomStartI, chart.zoomEndI, scaleX, scaleY, offsetX, offsetY, data, zoom.animation)
      zoomRendered = true
    }

    if (zoom.isMorphing && i >= zoom.minI && i < zoom.maxI) continue

    // TODO
    // if( zoomAnimation.value < 1 || data.chartType === CHART_KINDS.bar ) {
      var x = data.xColumn.data[i]
      var y = yColumn.data[i]
      ctx.lineTo(x * scaleX + offsetX, y * scaleY + offsetY)
    // }
  }


  ctx.stroke()
}
