const CLASS_PREFIX = require('./prefix')


export default function createElement(parent, tag, clazz) {
  var element = document.createElement(tag)
  if (clazz) {
    if( clazz.indexOf(CLASS_PREFIX) === -1 ) {
      clazz = CLASS_PREFIX + clazz
    }
    element.classList.add(clazz)
  }
  parent.appendChild(element)
  return element
}
