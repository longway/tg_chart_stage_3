export default function createAnimation(value, duration) {
  return {
    fromValue: value,
    toValue: value,
    value: value,
    startTime: 0,
    duration: duration,
    progress: 0,
  }
}
