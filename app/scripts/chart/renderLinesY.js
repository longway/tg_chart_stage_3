export default function renderLinesY(ctx, textY, toScreenY, minY, textCountY, width, paddingHor) {
  if (textY.alpha.value === 0) { return }

  ctx.globalAlpha = textY.alpha.value

  for (var i = 1; i <= textCountY; i++) {
    var y = toScreenY(minY.value + textY.delta * i)
    ctx.beginPath()
    ctx.moveTo(paddingHor, y)
    ctx.lineTo(width - paddingHor, y)
    ctx.stroke()
  }
}
