export default function renderStackedZoom(
  ctx,
  minI,
  maxI,
  chart,
  data,
  zoomAnimation,
  isPreview,
  selectI,
) {
  var columnWidth = Math.floor((data.intervalX / 24) * chart.scaleX) + 1
  var offsetX = chart.offsetX

  for (var i = minI; i < maxI; i++) {
    var x = Math.floor(data.xColumn.toZoomData[i] * chart.scaleX + offsetX)

    var from = chart.offsetY
    var isNotFaded = isPreview || selectI === -1 || i === selectI

    for (var j = 0; j < data.columns.length; j++) {
      var yColumn = data.columns[j]
      if (yColumn.alpha.value === 0) continue

      var y = (yColumn.toZoomData[i] * zoomAnimation.value + yColumn.fromZoomData[i] * (1 - zoomAnimation.value)) * chart.scaleY
      y *= yColumn.alpha.value
      y = Math.floor(y)

      ctx.fillStyle = isNotFaded ? data.colors[yColumn.name] : yColumn.fadedColor
      ctx.fillRect(x, from, columnWidth, y)

      from += y
    }
  }
}
