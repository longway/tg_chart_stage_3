import playAnimation from './playAnimation'
import { HOUR } from './timeConstants'


export default function updateTextsX(
  rangeX,
  textCountX,
  oldTextX,
  newTextX,
  time,
) {
  var delta = rangeX / HOUR / textCountX

  var pow = 1
  while (pow <= delta) pow *= 2
  delta = pow

  if (delta < newTextX.delta) {
    // NOTE: ADD DATES

    oldTextX.delta = newTextX.delta
    oldTextX.alpha.value = 1
    playAnimation(oldTextX.alpha, 1, time)

    newTextX.delta = delta
    newTextX.alpha.value = 0
    playAnimation(newTextX.alpha, 1, time)
  } else if (delta > newTextX.delta) {
    // NOTE: REMOVE DATES

    oldTextX.delta = newTextX.delta
    oldTextX.alpha.value = newTextX.alpha.value
    playAnimation(oldTextX.alpha, 0, time)

    newTextX.delta = delta
    newTextX.alpha.value = 1
    playAnimation(newTextX.alpha, 1, time)
  }
}
