const day = 86400000
const hour = day / 24

export const DAY = day
export const HOUR = hour
