export default function fillRoundCorners(ctx, x1, y1, x2, y2, radius, color) {
  ctx.beginPath()
  ctx.moveTo(x1, y1)
  ctx.lineTo(x1 + radius, y1)
  ctx.quadraticCurveTo(x1, y1, x1, y1 + radius)

  ctx.moveTo(x1, y2)
  ctx.lineTo(x1 + radius, y2)
  ctx.quadraticCurveTo(x1, y2, x1, y2 - radius)

  ctx.moveTo(x2, y1)
  ctx.lineTo(x2 - radius, y1)
  ctx.quadraticCurveTo(x2, y1, x2, y1 + radius)

  ctx.moveTo(x2, y2)
  ctx.lineTo(x2 - radius, y2)
  ctx.quadraticCurveTo(x2, y2, x2, y2 - radius)

  ctx.fillStyle = color
  ctx.fill()
}
