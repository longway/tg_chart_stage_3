import formatNumber from './formatNumber'


export default function renderTextsScaledY(
  ctx,
  columnNumber,
  textY,
  x,
  data,
  textCountY,
  toScreenY,
  minY,
  zoomOffsetY,
  textYMargin,
) {
  if (data.columns[columnNumber].alpha.toValue === 0) return
  ctx.fillStyle = data.colors[data.columns[columnNumber].name]
  for (var i = 0; i <= textCountY; i++) {
    var y = toScreenY(minY.value + textY.delta * i)
    var value = (minY.toValue - zoomOffsetY) / data.columns[columnNumber].scaleY + (textY.delta * i) / data.columns[columnNumber].scaleY
    ctx.fillText(formatNumber(value, true), x, y + textYMargin)
  }
}
