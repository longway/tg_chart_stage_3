'use strict'

import formatDate, { formatDateStyles } from './formatDate'
import fadeColor from './fadeColor'
import formatNumber from './formatNumber'
import addEventListener from './addEventListener'
import removeEventListener from './removeEventListener'
import removeAllChilds from './removeAllChilds'
import createElement from './createElement'
import drawRoundRect from './drawRoundRect'
import fillRoundCorners from './fillRoundCorners'
import createAnimation from './createAnimation'
import playAnimation from './playAnimation'
import updateAnimation from './updateAnimation'
import createAnimatedText from './createAnimatedText'
import renderTextsX from './renderTextsX'
import updateTextsX from './updateTextsX'
import renderTextsY from './renderTextsY'
import updateTextsY from './updateTextsY'
import renderLinesY from './renderLinesY'
import { DAY } from './timeConstants'
import { CHART_KINDS } from './CHART_KINDS'
import renderStacked from './renderStacked'
import renderPercentage from './renderPercentage'
import { PIXEL_RATIO } from './constants'
const CLASS_PREFIX = require('./prefix')
import renderPath from './renderPath'
import renderMainPath from './renderMainPath'


const WHITE_COLOR = '#ffffff'
const THEMES = {
  light: 'light',
  dark: 'dark',
}
var THEMES_COLORS = {}
THEMES_COLORS[THEMES.light] = {
  circleFill: WHITE_COLOR,
  line: 'rgba(24, 45, 59, 0.1)',
  zeroLine: '#ecf0f3',
  selectLine: '#dfe6eb',
  text: '#8E8E93',

  preview: '#E2EEF9',
  previewAlpha: 0.6,
  previewBorderAlpha: 1.0,
  previewBorder: '#C0D1E1',
  previewUiWTouchLine: WHITE_COLOR,
  previewUiWTouchAlpha: 1.0,

  background: WHITE_COLOR,
}
THEMES_COLORS[THEMES.dark] = {
  circleFill: '#242f3e',
  line: 'rgba(255, 255, 255, 0.1)',
  zeroLine: '#313d4d',
  selectLine: '#3b4a5a',
  text: 'rgba(163, 177, 194, 0.6)',

  preview: '#304259',
  previewAlpha: 0.6,
  previewBorderAlpha: 1.0,
  previewBorder: '#56626D',
  previewUiWTouchLine: WHITE_COLOR,
  previewUiWTouchAlpha: 1.0,

  background: '#242f3e',
}
const DURATIONS = {
  textXFade: 200,
  scale: 200,
  scalePreview: 200,
  zoom: 300,
  pieZoom: 200,
  pieMovePreviewTrimmer: 100,
  pieSliceResize: 150,
  move: 30,
}
const LONG_TAP_TIME = 1000
const ZOOM_IN_ANIMATION_HALF_WAY = 0.5
const DRAG_MODES = {
  none: 0,
  beginOfTrimmer: 1,
  endOfTrimmer: 2,
  centerOfTrimmer: 3,
}


const isSupportTouchEvents = 'ontouchstart' in window
var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1


function Chart(container) {
  container.classList.add(CLASS_PREFIX + 'chart')

  var titleContainer = createElement(container, 'div', 'titles-container')
  var title = createElement(titleContainer, 'div', 'chart-title')
  var zoomContainer = createElement(titleContainer, 'div', 'chart__zoom')
  createElement(zoomContainer, 'div', 'chart__zoom__icon')
  createElement(zoomContainer, 'div', 'chart__zoom__text').textContent = 'Zoom Out'

  var time = 0
  titleContainer = createAnimatedText(titleContainer, 'left', [title, zoomContainer])

  var datesTitle = createAnimatedText(createElement(container, 'div', 'title-dates'))

  var canvasesContainer = createElement(container, 'div', 'chart__canvases')
  var canvases = {
    chart: createElement(canvasesContainer, 'canvas', 'chart__canvas'),
    previewUi: createElement(canvasesContainer, 'canvas', 'chart__preview-ui'),
  }
  var contexts = {
    chart: canvases.chart.getContext('2d'),
    previewUi: canvases.previewUi.getContext('2d'),
  }

  var checksContainer = createElement(container, 'div', 'chart__checks')
  var popup = {
    node: createElement(container, 'div', 'chart__popup'),
    width: 0,
    height: 0,
    maxHeight: 0,
    isShowed: true,
    isScrollable: false,
    isTouching: false,
    isScrolled: false,
    x: 0,
    y: 0,
    columns: null,
    values: null,
    percents: null,
    updateSizes: function updateSizes() {
      var bounds = this.node.getBoundingClientRect()
      this.width = bounds.width
      this.height = bounds.height
      this.maxHeight = (mainChart.current.height - mainChartPaddingTop - 10 * PIXEL_RATIO) / PIXEL_RATIO
      this.isScrollable = this.contentContainer.scrollHeight > this.maxHeight
    },
    hide: function hide() {
      if( !this.isShowed ) { return }

      this.isShowed = false
      this.node.classList.remove(CLASS_PREFIX + 'visible')
      this.node.classList.add(CLASS_PREFIX + 'hidden')

      if( this.title ) { this.title.clearTexts() }
    },
    show: function show() {
      if( this.isShowed ) { return }

      this.isShowed = true
      this.node.classList.add(CLASS_PREFIX + 'visible')
      this.node.classList.remove(CLASS_PREFIX + 'hidden')
      this.updateSizes()
      this.contentContainer.style.maxHeight = this.maxHeight * (this.isScrollable ? 0.5 : 1) + 'px'
    },
    contentContainer: null,
    createTitle: function createTitle() {
      this.title = {
        node: createAnimatedText(createElement(popup.node, 'div', 'chart__popup__title'), 'left'),
        isShowed: true,
        hide: function hide() {
          if( !this.isShowed ) { return }

          this.isShowed = false
          this.node.container.classList.remove(CLASS_PREFIX + 'visible')
          this.node.container.classList.add(CLASS_PREFIX + 'hidden')
        },
        show: function show() {
          if( this.isShowed ) { return }

          this.isShowed = true
          this.node.container.classList.remove(CLASS_PREFIX + 'hidden')
          this.node.container.classList.add(CLASS_PREFIX + 'visible')
        },
        clearTexts: function clearTexts() {
          this.node.clearTexts()
        }
      }
    },
    createArrowRight: function createArrowRight() {
      this.arrowRight = {
        node: createElement(popup.node, 'div', 'chart__popup__arrow-right'),
        isShowed: true,
        hide: function hide() {
          if( !this.isShowed ) { return }

          this.isShowed = false
          this.node.classList.remove(CLASS_PREFIX + 'visible')
          this.node.classList.add(CLASS_PREFIX + 'hidden')
        },
        show: function show() {
          if( this.isShowed ) { return }

          this.isShowed = true
          this.node.classList.remove(CLASS_PREFIX + 'hidden')
          this.node.classList.add(CLASS_PREFIX + 'visible')
        }
      }
    },
    createContentContainer: function createContentContainer() {
      this.contentContainer = createElement(popup.node, 'div', 'chart__popup__content')
    },
    init: function init() {
      this.createTitle()
      this.createArrowRight()
      this.createContentContainer()
    }
  }
  popup.hide()

  var popupTopMargin = 30 * PIXEL_RATIO
  var piePopupTopMargin = 20 * PIXEL_RATIO
  var popupTextYWidth = 15 * PIXEL_RATIO
  var leftMarginFromPopup = 10 * PIXEL_RATIO

  var width = 0
  var height = 0

  var data = {
    current: null,
    old: null,
  }

  var textCount = {
    x: 6,
    y: 5,
  }

  var previewVerticalHalfLineW = 2 * PIXEL_RATIO
  var previewLineH = 10 * PIXEL_RATIO
  var previewVerticalInsideLineW = 1 * PIXEL_RATIO

  var previewMarginTop = 40 * PIXEL_RATIO
  var mouseArea = 20 * PIXEL_RATIO
  var previewUi = {
    width: 10 * PIXEL_RATIO,
    height: 2 * PIXEL_RATIO,
    radius: 10 * PIXEL_RATIO,
    min: 0,
    max: 0,
  }
  var lineWidth = 1 * PIXEL_RATIO
  var previewLineWidth = 1 * PIXEL_RATIO
  var circle = {
    radius: 3 * PIXEL_RATIO,
    lineWidth: 4 * PIXEL_RATIO,
  }
  var DEFAULT_FONT = `${ 11 * PIXEL_RATIO }px Arial`
  var textY = {
    margin: -6 * PIXEL_RATIO,
    height: 50 * PIXEL_RATIO,
    old: {
      delta: 1,
      alpha: createAnimation(0, DURATIONS.scale),
    },
    new: {
      delta: 1,
      alpha: createAnimation(0, DURATIONS.scale),
    },
  }

  var textX = {
    margin: 18 * PIXEL_RATIO,
    width: 30 * PIXEL_RATIO,
    old: {
      delta: 1,
      alpha: createAnimation(0, DURATIONS.textXFade)
    },
    new: {
      delta: 1,
      alpha: createAnimation(0, DURATIONS.textXFade)
    },
  }

  var paddingHor = 16 * PIXEL_RATIO

  var mainChart = {
    current: createChart(),
    old: null,
  }
  var mainChartPaddingTop = 21 * PIXEL_RATIO

  var previewChart = createChart()
  previewChart.height = 44 * PIXEL_RATIO + 2 * previewUi.height

  var mouseSelect = {
    x: 0,
    y: 0,
    i: 0,
  }

  var pieCenter = {
    x: 0,
    y: 0,
  }
  var pieSelectColumn = {
    current: -1,
    prev: -1,
  }
  var pieSelectColumnDelta = {
    current: createAnimation(0, DURATIONS.scale),
    prev: createAnimation(0, DURATIONS.scale),
  }
  var pieAnimationMove = 10 * PIXEL_RATIO


  var needRedraw = {
    main: true,
    preview: true,
    previewUi: true,
  }

  var canvasBounds = {
    top: 0,
    left: 0,
  }

  var mouse = {
    mode: DRAG_MODES.none,
    x: 0,
    y: 0,
    startX: 0,
    range: 0,
  }

  var zoom = {
    isActive: false,
    isZoomingIn: false,
    isMorphing: false,
    i: -1,
    minI: -1,
    maxI: -1,
    offsetY: 0,
    animation: createAnimation(0, DURATIONS.zoom),
  }

  var userHasScrolled = false

  var lastRenderAt = null
  var onResizeCheckedAt = null
  var isWindowResized = false

  var longTapTimeoutId = null
  var shouldHandleCheckboxMouseUp = true
  var checkboxesShakeTimeouts = null

  var theme = THEMES.light
  var isThemeLight = true

  var x_on_zoom = null
  var isTitleSet = false

  var isAnimationEnabled = true
  var isRenderedFirstTime = false

  var htmlNode = document.querySelector('html')

  var canvasCursor = 'default'

  var isFullMonth = true

  this.injectData = injectData
  this.destroy = destroy
  var isDestroyed = false

  initEventListeners()
  requestAnimationFrame(render)


  function createChart() {
    const chart = {
      isVisible: true,
      height: 0,
      rangeX: 0,
      rangeY: 0,
      scaleX: 1,
      scaleY: 1,
      offsetX: 0,
      offsetY: 0,

      minX: createAnimation(0, DURATIONS.scale),
      minY: createAnimation(0, DURATIONS.scale),
      maxX: createAnimation(0, DURATIONS.scale),
      maxY: createAnimation(0, DURATIONS.scale),

      minI: 0,
      maxI: 0,
      saveMinX: 0,
      saveMaxX: 0,
    }

    chart.toScreenX = function toScreenX(x) {
      return x * chart.scaleX + chart.offsetX
    }

    chart.toScreenY = function toScreenY(y) {
      return y * chart.scaleY + chart.offsetY
    }

    chart.fromScreenX = function fromScreenX(screenX) {
      return (screenX - chart.offsetX) / chart.scaleX
    }

    chart.save = function save() {
      chart.saveMinX = chart.minX.value
      chart.saveMaxX = chart.maxX.value
    }

    chart.restore = function restore() {
      setMinMaxX(chart, chart.saveMinX, chart.saveMaxX, DURATIONS.zoom)
    }

    return chart
  }

  function onMouseDown(e) {
    if( popup.isTouching || popup.isScrolled ) { return }

    mouse.x = (e.clientX - canvasBounds.left) * PIXEL_RATIO
    mouse.y = (e.clientY - canvasBounds.top) * PIXEL_RATIO

    if( isMouseDragInPreview() ) { return }
    detectIsInMainAndTryZoomIfDesktop()
  }

  function onMouseDownChecks(e) {
    clearLongTapTimeoutId()
    longTapTimeoutId = setTimeout(function tryFireLongTapTimeout() {
      tryFireLongTap(e)
    }, LONG_TAP_TIME)
  }

  function onTouchDownChecks(e) {
    onMouseDownChecks(e)
  }

  function clearLongTapTimeoutId() {
    clearTimeout(longTapTimeoutId)
  }

  function tryFireLongTap(e) {
    clearLongTapTimeoutId()
    if( !(e && e.target) ) { return }
    if( !((e.which || e.keyCode) === 1 || isSupportTouchEvents) ) { return }

    let checkbox = e.target
    while( checkbox && checkbox.nodeName !== 'BODY' && !checkbox.classList.contains(CLASS_PREFIX + 'chart__checkbox') ) {
      checkbox = checkbox.parentNode
    }

    if( checkbox == null || checkbox.nodeName === 'BODY' ) { return }

    if( !isAndroid ) {
      shouldHandleCheckboxMouseUp = false
    }

    var columnId = checkbox.getAttribute('data-id')

    toggleCheckboxes(columnId, true)
  }

  function isMouseInMain() {
    return (mouse.y > 0) && (mouse.y < height - previewChart.height) && (mouse.x > 0) && (mouse.x < width)
  }

  function isMouseInPreview() {
    if( !previewChart.isVisible ) {
      return false
    }

    return (
      (mouse.y > height - previewChart.height) &&
      (mouse.y < height) && (mouse.x > -mouseArea)
      && (mouse.x < width + mouseArea)
    )
  }

  function isMouseInLeftOfTrimmer() {
    return mouse.x > previewUi.min - mouseArea && mouse.x < previewUi.min + mouseArea / 2
  }

  function isMouseInRightOfTrimmer() {
    return mouse.x > previewUi.max - mouseArea / 2 && mouse.x < previewUi.max + mouseArea
  }

  function isMouseInCenterOfTrimmer() {
    return mouse.x > previewUi.min + mouseArea / 2 && mouse.x < previewUi.max - mouseArea / 2
  }

  function isMouseDragInPreview() {
    var inPreview = isMouseInPreview()
    if (inPreview) {
      if (isMouseInLeftOfTrimmer()) {
        mouse.mode = DRAG_MODES.beginOfTrimmer
      } else if (isMouseInRightOfTrimmer()) {
        mouse.mode = DRAG_MODES.endOfTrimmer
      } else if (isMouseInCenterOfTrimmer()) {
        mouse.mode = DRAG_MODES.centerOfTrimmer

        mouse.startX = previewUi.min - mouse.x
        mouse.range = mainChart.current.maxX.toValue - mainChart.current.minX.toValue
        setCanvasCursor('grabbing')
      }
    }

    return inPreview
  }

  function detectIsInMainAndTryZoomIfDesktop() {
    var inMain = isMouseInMain()
    if (inMain && !isSupportTouchEvents) {
      onZoomIn()
    }
  }

  function onTouchDown(e) {
    onMouseDown(e.touches[0], true)
  }

  function onMouseMove(e) {
    if( popup.isTouching || popup.isScrolled ) { return }

    mouse.x = (e.clientX - canvasBounds.left) * PIXEL_RATIO
    mouse.y = (e.clientY - canvasBounds.top) * PIXEL_RATIO

    var newCanvasCursor

    detectAndSetCanvasCursor()
  }

  function setCanvasCursor(newCanvasCursor) {
    if( newCanvasCursor === canvasCursor ) { return }

    canvasCursor = newCanvasCursor
    canvasesContainer.style.cursor = canvasCursor
  }

  function detectAndSetCanvasCursor() {
    if( isSupportTouchEvents ) { return }
    if( mouse.mode !== DRAG_MODES.none ) { return }

    if( isMouseInMain() ) {
      var newCanvasCursor
      if( zoom.isActive ) {
        newCanvasCursor = 'default'
      } else {
        newCanvasCursor = 'pointer'
      }
      setCanvasCursor(newCanvasCursor)
      return
    }

    if ( isMouseInPreview() ) {
      if (isMouseInLeftOfTrimmer() || isMouseInRightOfTrimmer()) {
        setCanvasCursor('ew-resize')
        return
      } else if (isMouseInCenterOfTrimmer()) {
        setCanvasCursor('grab')
        return
      }
    }

    setCanvasCursor('default')
  }

  function onTouchMove(e) {
    onMouseMove(e.touches[0])
  }

  function onMouseUp(e) {
    mouse.mode = DRAG_MODES.none
  }

  function isZoomAllowed() {
    return data.current.chartType === CHART_KINDS.percentage || !!x_on_zoom
  }

  function onZoomIn() {
    if (!zoom.isActive && mouse.mode === DRAG_MODES.none) {
      if( !isZoomAllowed() ) {
        return
      }

      zoom.isZoomingIn = true
      zoom.isActive = true

      titleContainer.toggle()
      popup.arrowRight.hide()

      zoom.i = mouseSelect.i
      if (data.current.chartType !== CHART_KINDS.bar) {
        if (zoom.i < 4) zoom.i = 4
        if (zoom.i > data.current.xColumn.data.length - 5) zoom.i = data.current.xColumn.data.length - 5
      }
      mainChart.current.save()
      previewChart.save()

      if (data.current.chartType === CHART_KINDS.percentage) {
        let colIStart = mouseSelect.i - 3
        if( colIStart < 1 ) { colIStart = 1 }

        let d = Object.assign({}, data.current.originalData)
        for( var i = 0; i < d.columns.length; i++ ) {
          d.columns[i] = d.columns[i].slice(0,1).concat( d.columns[i].slice(colIStart, colIStart + 7) )
        }
        injectData(d)
        detectAndSetCanvasCursor()
        return
      }

      var x = data.current.xColumn.data[zoom.i]
      x_on_zoom(x)
        .then(function(d) {
          injectData(d)
          detectAndSetCanvasCursor()
        })

    }
  }

  function onZoomOut() {
    if (!zoom.isActive) { return }

    zoom.isActive = false
    zoom.offsetY = 0
    popup.arrowRight.show()

    updateMinMaxY(mainChart.current)
    updateMinMaxY(previewChart)

    if (zoom.isMorphing) {
      mainChart.current.restore()
      previewChart.restore()
      playAnimation(zoom.animation, 0, time)

      if (data.current.chartType === CHART_KINDS.pie) {
        data.current.chartType = CHART_KINDS.percentage
        popup.title.show()
        popup.title.node.container.classList.remove(CLASS_PREFIX + 'hidden-with-display')
      }
    } else {
      injectData(data.old.originalData, true)
    }

    titleContainer.toggleReverse()
  }

  function onWindowResize() {
    isWindowResized = true
  }

  function onScroll() {
    if( !userHasScrolled ) {
      userHasScrolled = true
    }
  }

  function onTouchStartPopup() {
    popup.isTouching = true
  }

  function onTouchMovePopup() {
    popup.isScrolled = true
  }

  function onTouchEndPopup() {
    if( !popup.isScrolled ) {
      onZoomIn()
    }
    popup.isTouching = false
    popup.isScrolled = false
  }

  function checkThemeOnHtmlNode() {
    var isDark = htmlNode.classList.contains('dark')
    setTheme(isDark)
  }

  function setTheme(isDark) {
    isThemeLight = !isDark
    theme = isThemeLight ? THEMES.light : THEMES.dark

    if( isThemeLight ) {
      container.classList.remove(CLASS_PREFIX + 'chart--dark')
    } else {
      container.classList.add(CLASS_PREFIX + 'chart--dark')
    }

    needRedraw.main = needRedraw.preview = needRedraw.previewUi = true
  }

  function injectData(d, zoomOut) {
    if( d.x_on_zoom && !x_on_zoom ) {
      x_on_zoom = d.x_on_zoom
    }

    var newChartType

    if (zoom.isActive && d.percentage) {
      newChartType = CHART_KINDS.pie
    } else {
      if (d.y_scaled) newChartType = CHART_KINDS.yScaled
      else if (d.percentage) newChartType = CHART_KINDS.percentage
      else if (d.stacked) newChartType = CHART_KINDS.stacked
      else newChartType = CHART_KINDS.line

      if( !d.stacked ) {
        for (var name in d.types) {
          if (d.types[name] === 'bar') {
            newChartType = CHART_KINDS.bar
            break
          }
        }
      }
    }

    if( !isTitleSet ) {
      isTitleSet = true
      let _title = d.title
      if( !_title ) {
        _title = CHART_KINDS.names[ newChartType.toString() ]
      }
      title.textContent = _title
    }

    if (zoomOut) {
      setMinMaxX(mainChart.current, mainChart.old.saveMinX, mainChart.old.saveMaxX, DURATIONS.zoom) // lines to bar
      playAnimation(zoom.animation, 0, time)
    } else if (zoom.isActive) {
      if( newChartType === CHART_KINDS.pie ) {
        zoom.animation.duration = DURATIONS.pieZoom
        popup.title.hide()
        playAnimation(zoom.animation, 1, time)
        playAnimation(textX.new.alpha, 0, time)
        playAnimation(textY.new.alpha, 0, time)
      } else {
        setMinMaxX(mainChart.current, data.current.xColumn.data[zoom.i] - data.current.intervalX / 2, data.current.xColumn.data[zoom.i] + data.current.intervalX / 2, DURATIONS.zoom) // bar to lines
        zoom.animation.duration = 10
        zoom.animation.halfWayTo = 1
        playAnimation(zoom.animation, ZOOM_IN_ANIMATION_HALF_WAY, time)
      }
    }

    zoom.isMorphing = zoom.isActive && (data.current.chartType === newChartType || newChartType === CHART_KINDS.pie)

    if (zoom.isMorphing) {
      zoom.minI = zoom.i - 3
      zoom.maxI = zoom.i + 4

      data.current.xColumn.toZoomData = d.columns[0]

      for (var i = 0; i < data.current.columns.length; i++) {
        for (var j = 0; j < data.current.xColumn.toZoomData.length; j++) {

          var convertI = zoom.minI + Math.floor(j / 24)
          if (data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) {
            data.current.columns[i].toZoomData[j] = d.columns[i + 1][j]
            data.current.columns[i].fromZoomData[j] = data.current.columns[i].data[convertI]
          } else {
            zoom.offsetY = data.current.columns[0].data[zoom.minI]
            data.current.columns[i].toZoomData[j] = d.columns[i + 1][j] + zoom.offsetY / data.current.columns[i].scaleY
            var di = j % 24
            var y = data.current.columns[i].data[convertI] + (data.current.columns[i].data[convertI + 1] - data.current.columns[i].data[convertI]) * (di / 24)
            data.current.columns[i].fromZoomData[j] = y
          }
        }
      }

      setMinMaxX(mainChart.current, data.current.xColumn.data[zoom.i], data.current.xColumn.data[zoom.i] + data.current.intervalX, DURATIONS.zoom)
      setMinMaxX(previewChart, data.current.xColumn.data[zoom.minI], data.current.xColumn.data[zoom.maxI], DURATIONS.zoom)
    } else {
      setData(d, newChartType)
    }
  }

  function updateCheckboxStyle(checkbox) {
    var id = checkbox.getAttribute('data-id')
    var column = data.current.columns[id]
    var checked = column.checked

    if (checked) {
      checkbox.classList.add(CLASS_PREFIX + 'chart__checkbox--checked')
      checkbox.style.borderColor = checkbox.style.backgroundColor = data.current.colors[column.name]
      checkbox.style.color = ''
    } else {
      checkbox.classList.remove(CLASS_PREFIX + 'chart__checkbox--checked')
      checkbox.style.backgroundColor = ''
      checkbox.style.borderColor = checkbox.style.color = data.current.colors[column.name]
    }
  }

  function toggleCheckboxesColumns(_columns, forceChecked) {
    let changed = false
    for( let _i = 0; _i < _columns.length; _i++ ) {
      let _column = _columns[_i]

      const checkboxNode = checksContainer.querySelector(`[data-id="${_column.id}"]`)

      const checked = forceChecked ? true : _column.checked

      checked = !checked

      if( checked !== _column.checked ) {
        var checkedColumn = data.current.columns[_column.id]
        checkedColumn.saveScaleY = previewChart.scaleY
        checkedColumn.saveOffsetY = previewChart.offsetY
        checkedColumn.checked = checked
        updateCheckboxStyle(checkboxNode)

        playAnimation(checkedColumn.alpha, checked ? 1 : 0, time)
        playAnimation(checkedColumn.previewAlpha, checked ? 1 : 0, time)

        changed = true
      }
    }

    return changed
  }

  function toggleCheckboxes(columnId, invert = false) {
    const checkedColumn = data.current.columns[columnId]

    let { checked } = checkedColumn

    let checkboxNode = checksContainer.querySelector(`[data-id="${columnId}"]`)

    let _columns
    if( invert ) {
      _columns = data.current.columns.filter(_column => _column.id !== columnId && _column.checked)
    } else {
      _columns = [data.current.columns[columnId]]
    }

    let shouldShake = checked && (
      !invert && data.current.columns.filter(_column => _column.checked && _column.id !== checkedColumn.id).length === 0 ||
      _columns.length === 0
    )
    if( shouldShake ) {
      clearTimeout(checkboxesShakeTimeouts[columnId])
      checkboxNode.classList.remove(CLASS_PREFIX + 'chart__checkbox--shake')
      setTimeout(() => {
        checkboxNode.classList.add(CLASS_PREFIX + 'chart__checkbox--shake')

        checkboxesShakeTimeouts[columnId] = setTimeout(function() {
          checkboxNode.classList.remove(CLASS_PREFIX + 'chart__checkbox--shake')
        }, 500)
      }, 0)
      return
    }

    var changed = toggleCheckboxesColumns(_columns, checked && invert)
    if( !checked && invert ) {
      _columns = [data.current.columns[columnId]]
      toggleCheckboxesColumns(_columns)
      changed = true
    }

    if( !changed ) { return }

    // setIsAnimationEnabled()
    needRedraw.main = needRedraw.preview = true
    updateMinMaxY(previewChart)
    updateMinMaxY(mainChart.current)
  }

  function handleClickCheckbox(e) {
    if( !shouldHandleCheckboxMouseUp ) {
      shouldHandleCheckboxMouseUp = true
      return
    }
    clearLongTapTimeoutId()

    var id = e.currentTarget.getAttribute('data-id')
    toggleCheckboxes(id)
  }

  function setData(newData, newChartType) {
    function findNameOfX(types) {
      for (var name in types) {
        if (types[name] === 'x') return name
      }
      return null
    }

    data.old = data.current
    mainChart.old = mainChart.current

    popup.columns = []
    popup.values = []
    popup.percents = []

    checkboxesShakeTimeouts = []

    removeAllChilds(checksContainer)
    removeAllChilds(popup.node)

    popup.init()

    if (newData.columns.length < 2 || newData.columns[0].length < 3) {
      data.current = null
      return
    }

    data.current = {
      columns: [],
      colors: newData.colors,
      names: newData.names,
      chartType: newChartType,
      originalData: newData,
    }
    mainChart.current = createChart()

    var nameOfX = findNameOfX(newData.types)
    var maxRange = Number.MIN_VALUE

    for (var i = 0; i < newData.columns.length; i++) {
      var columnData = newData.columns[i]
      var name = columnData[0]
      var column = {
        name,
        data: columnData,
        fromZoomData: [],
        toZoomData: [],
        min: columnData[1],
        max: columnData[1],
        alpha: createAnimation(1, DURATIONS.scale),
        previewAlpha: createAnimation(1, DURATIONS.scalePreview),
        checked: true,
      }

      if (name === nameOfX) {
        column.min = columnData[1]
        column.max = columnData[columnData.length - 1]
        data.current.xColumn = column
      } else {
        column.fadedColor = fadeColor(data.current.colors[name])

        for (var j = 2; j < columnData.length; j++) {
          var value = columnData[j]
          if (value < column.min) column.min = value
          else if (value > column.max) column.max = value
        }
        column.id = data.current.columns.length.toString()
        data.current.columns.push(column)

        if (maxRange < (column.max - column.min)) {
          maxRange = (column.max - column.min)
        }

        // create checkbox

        if (newData.columns.length > 2) {
          var checkboxNode = createElement(checksContainer, 'label', 'chart__checkbox')

          checkboxNode.setAttribute('data-id', column.id)
          addEventListener(checkboxNode, 'click', handleClickCheckbox)

          createElement(checkboxNode, 'span', 'chart__checkbox__symbol')

          createElement(checkboxNode, 'span').textContent = data.current.names[name]

          updateCheckboxStyle(checkboxNode)
        }


        var popupColumn = createElement(popup.contentContainer, 'div', 'chart__popup__column')
        popup.columns.push(popupColumn)

        if (data.current.chartType === CHART_KINDS.percentage) {
          popup.percents.push(createAnimatedText(createElement(popupColumn, 'div', 'chart__popup__percent'), 'right'))
        }

        var popupLabel = createElement(popupColumn, 'div', 'chart__popup__label')
        popupLabel.textContent = data.current.names[name]

        var popupValue = createElement(popupColumn, 'div', 'chart__popup__value')
        popupValue.style.color = data.current.colors[name]
        popup.values.push(createAnimatedText(popupValue))
      }
    }
    // setIsAnimationEnabled()

    if ((data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) && data.current.columns.length > 1) {
      var popupColumn = createElement(popup.contentContainer, 'div', 'chart__popup__column')
      popup.columns.push(popupColumn)
      var popupLabel = createElement(popupColumn, 'div', 'chart__popup__label')
      popupLabel.textContent = 'Total'
      var popupValue = createElement(popupColumn, 'div', 'chart__popup__value')
      popup.values.push(createAnimatedText(popupValue))
    }

    for (var i = 0; i < data.current.columns.length; i++) {
      var column = data.current.columns[i]
      if (data.current.chartType === CHART_KINDS.yScaled) {
        column.scaleY = maxRange / (column.max - column.min)
      } else {
        column.scaleY = 1
      }
    }

    onResize(true)

    data.current.intervalX = data.current.xColumn.data[2] - data.current.xColumn.data[1]
    setMinMaxX(previewChart, data.current.xColumn.data[1], data.current.xColumn.data[data.current.xColumn.data.length - 1])
    initXAndYOfChart(previewChart)

    zoom.isActive || !isZoomAllowed() ? popup.arrowRight.hide() : popup.arrowRight.show()

    if (zoom.isActive) {
      setMinMaxX(mainChart.current, mainChart.old.saveMinX, mainChart.old.saveMaxX)
    } else if (mainChart.old && mainChart.old.saveMaxX !== 0) {
      setMinMaxX(mainChart.current, data.current.xColumn.data[zoom.i] - data.current.intervalX / 2, data.current.xColumn.data[zoom.i] + data.current.intervalX / 2)
    } else {
      setMinMaxX(mainChart.current, data.current.xColumn.max - (data.current.xColumn.max - data.current.xColumn.min) / 4, data.current.xColumn.max)
    }
    initXAndYOfChart(mainChart.current)

    if (zoom.isActive) {
      mainChart.current.saveMinX = mainChart.old.saveMinX
      mainChart.current.saveMaxX = mainChart.old.saveMaxX
      setMinMaxX(mainChart.current, data.current.xColumn.min, data.current.xColumn.max, DURATIONS.zoom)
    } else if (mainChart.old && mainChart.old.saveMaxX !== 0) {
      setMinMaxX(mainChart.current, mainChart.old.saveMinX, mainChart.old.saveMaxX, DURATIONS.zoom)
    }

    needRedraw.main = needRedraw.preview = needRedraw.previewUi = true
  }

  // NOTE: PREVENT FIRST ANIMATION ON LOAD
  function initXAndYOfChart(chart) {
    chart.minX.value = chart.minX.toValue
    chart.maxX.value = chart.maxX.toValue
    onChangeX(chart)

    chart.minY.value = chart.minY.toValue
    chart.maxY.value = chart.maxY.toValue
    onChangeY(chart)
  }

  function updateDatesTitles() {
    var diff = mainChart.current.maxX.toValue - mainChart.current.minX.toValue
    if (diff <= DAY) {
      var startDate = formatDate(mainChart.current.minX.toValue, formatDateStyles.title, zoom.isActive, isFullMonth)
      // TOREMOVE ?
      // if( mainChart.current.maxX.toValue === previewChart.maxX.toValue ) {
      //   datesTitle.setText(endDate, time)
      // } else

      if( mainChart.current.minX.toValue === previewChart.minX.toValue ) {
        datesTitle.setText(startDate, time)
      } else  {
        datesTitle.setText(startDate, time)
      }
    } else {
      var startDate = formatDate(mainChart.current.minX.toValue, formatDateStyles.title, zoom.isActive, isFullMonth)
      var endDate = formatDate(mainChart.current.maxX.toValue, formatDateStyles.title, zoom.isActive, isFullMonth)
      datesTitle.setText(startDate + ' - ' + endDate, time)
    }
  }

  function setMinMaxX(chart, min, max, duration) {
    chart.minX.duration = chart.maxX.duration = (duration || DURATIONS.move)

    var changed = false

    if (min !== null && chart.minX.toValue !== min) {
      playAnimation(chart.minX, min, time)
      changed = true
    }

    if (max !== null && chart.maxX.toValue !== max) {
      playAnimation(chart.maxX, max, time)
      changed = true
    }

    if (changed && chart === mainChart.current) {
      if( data.current.chartType === CHART_KINDS.pie ) {
        for (var i = 0; i < data.current.columns.length; i++) {
          var yColumn = data.current.columns[i]
          var total = 0
          for (var j = mainChart.current.minI; j < mainChart.current.maxI; j++) {
            total += yColumn.data[j]
          }

          if( yColumn.total ) {
            playAnimation(yColumn.total, total, time)
          } else {
            yColumn.total = createAnimation(total, DURATIONS.pieSliceResize)
          }
        }
      }

      updateDatesTitles()
    }
  }

  function onChangeX(chart) {
    chart.rangeX = chart.maxX.value - chart.minX.value
    chart.scaleX = (width - paddingHor * 2) / chart.rangeX
    chart.offsetX = -chart.minX.value * chart.scaleX + paddingHor

    var padding = chart === previewChart ? 0 : paddingHor / chart.scaleX

    // I

    chart.minI = Math.floor((chart.minX.value - data.current.xColumn.min - padding) / data.current.intervalX) + 1
    if (chart.minI < 1) chart.minI = 1

    chart.maxI = Math.ceil((chart.maxX.value - data.current.xColumn.min + padding) / data.current.intervalX) + 2
    if (chart.maxI > data.current.xColumn.data.length) chart.maxI = data.current.xColumn.data.length

    if (zoom.isMorphing) {
      chart.zoomStartI = Math.floor((chart.minX.value - data.current.xColumn.data[zoom.minI] - padding) / (data.current.intervalX / 24))
      if (chart.zoomStartI < 1) chart.zoomStartI = 1

      chart.zoomEndI = Math.ceil((chart.maxX.value - data.current.xColumn.data[zoom.minI] + padding) / (data.current.intervalX / 24)) + 2
      if (chart.zoomEndI > data.current.xColumn.toZoomData.length) chart.zoomEndI = data.current.xColumn.toZoomData.length
    }

    // toI

    var toChanged = false

    var newToMinI = Math.floor((chart.minX.toValue - data.current.xColumn.min - padding) / data.current.intervalX) + 1
    if (newToMinI < 1) newToMinI = 1
    if (chart.toMinI !== newToMinI) {
      chart.toMinI = newToMinI
      toChanged = true
    }

    var newToMaxI = Math.ceil((chart.maxX.toValue - data.current.xColumn.min + padding) / data.current.intervalX) + 2
    if (newToMaxI > data.current.xColumn.data.length) newToMaxI = data.current.xColumn.data.length
    if (chart.toMaxI !== newToMaxI) {
      chart.toMaxI = newToMaxI
      toChanged = true
    }

    if (zoom.isMorphing) {
      var newToZoomStartI = Math.floor((chart.minX.toValue - data.current.xColumn.data[zoom.minI] - padding) / (data.current.intervalX / 24))
      if (newToZoomStartI < 1) newToZoomStartI = 1
      if (chart.toZoomStartI !== newToZoomStartI) {
        chart.toZoomStartI = newToZoomStartI
        toChanged = true
      }

      var newToZoomEndI = Math.ceil((chart.maxX.toValue - data.current.xColumn.data[zoom.minI] + padding) / (data.current.intervalX / 24)) + 2
      if (newToZoomEndI > data.current.xColumn.toZoomData.length) newToZoomEndI = data.current.xColumn.toZoomData.length
      if (chart.toZoomEndI !== newToZoomEndI) {
        chart.toZoomEndI = newToZoomEndI
        toChanged = true
      }
    }

    if (toChanged) {
      updateMinMaxY(chart)
    }

    //

    previewUi.min = previewChart.toScreenX(mainChart.current.minX.value)
    previewUi.max = previewChart.toScreenX(mainChart.current.maxX.value)

    needRedraw.main = needRedraw.previewUi = true

    if (chart === mainChart.current) {
      updateTextsX(mainChart.current.rangeX, textCount.x, textX.old, textX.new, time)
    }
  }

  function formatMinY(minY) {
    if( minY < 10 ) { return minY }

    let k = 10

    while( minY > k * 10 ) {
      k *= 10
    }

    if( minY < 10 * k ) {
      minY = Math.floor(minY / k) * k
    }

    return minY
  }

  function formatMaxY(maxY, minY = 0) {
    maxY = Math.ceil(maxY)
    if( maxY < 100 ) {
      return maxY
    }

    let diff = maxY - minY
    let k = 10

    while( diff / k > 1 ) {
      k *= 10
    }
    k /= 100

    var iterations = 0

    while( (diff + minY) / 5 % k > 0 ) {
      diff = Math.ceil(diff / k) * k + k

      iterations++
      if( iterations > 100 ) {
        console.log('TODO fix iterations', iterations, maxY, minY, diff)
        return maxY
      }
    }
    maxY = minY + diff

    return maxY
  }

  function updateMinMaxY(chart) {
    var newMinY
    var newMaxY

    if (data.current.chartType === CHART_KINDS.percentage || data.current.chartType === CHART_KINDS.pie) {

      newMinY = 0
      newMaxY = 100

    } else if (data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) {

      var maxSum = 0
      for (var i = chart.toMinI; i < chart.toMaxI; i++) {
        if (zoom.isMorphing && i >= zoom.minI - 1 && i <= zoom.maxI + 1) continue
        var sum = 0

        for (var j = 0; j < data.current.columns.length; j++) {
          var yColumn = data.current.columns[j]
          if (yColumn.alpha.toValue === 0) continue
          sum += yColumn.data[i]
        }

        if (maxSum < sum) maxSum = sum
      }

      if (zoom.isMorphing) {
        for (var i = chart.toZoomStartI; i < chart.toZoomEndI; i++) {
          var sum = 0

          for (var j = 0; j < data.current.columns.length; j++) {
            var yColumn = data.current.columns[j]
            if (yColumn.alpha.toValue === 0) continue
            var y = yColumn.toZoomData[i]
            sum += y
          }

          if (maxSum < sum) maxSum = sum
        }
      }

      newMinY = 0
      newMaxY = formatMaxY(maxSum)

    } else {
      newMinY = Number.MAX_VALUE
      newMaxY = Number.MIN_VALUE

      for (var i = 0; i < data.current.columns.length; i++) {
        var column = data.current.columns[i]
        if (column.alpha.toValue === 0) continue

        column.minY = Number.MAX_VALUE
        column.maxY = Number.MIN_VALUE

        for (var j = chart.toMinI; j < chart.toMaxI; j++) {
          if (zoom.isMorphing && j >= zoom.minI - 1 && j <= zoom.maxI + 1) continue
          var y = column.data[j] * column.scaleY
          if (y < column.minY) column.minY = y
          if (y > column.maxY) column.maxY = y
        }

        column.minY = formatMinY(column.minY)
        column.maxY = formatMaxY(column.maxY, column.minY === Number.MIN_VALUE ? 0 : column.minY)

        if (column.minY < newMinY) newMinY = column.minY
        if (column.maxY > newMaxY) newMaxY = column.maxY
      }

      if (zoom.isMorphing) {
        for (var i = 0; i < data.current.columns.length; i++) {
          var column = data.current.columns[i]
          if (column.alpha.toValue === 0) continue

          for (var j = chart.toZoomStartI; j < chart.toZoomEndI; j++) {
            var y = column.toZoomData[j] * column.scaleY
            if (y < column.minY) column.minY = y
            if (y > column.maxY) column.maxY = y
          }

          if (column.minY < newMinY) newMinY = column.minY
          if (column.maxY > newMaxY) newMaxY = column.maxY
        }
      }

      // if( newMinY !== Number.MAX_VALUE && !zoom.isActive ) {
      //   newMinY = newMinY
      // }

      if (newMaxY === Number.MIN_VALUE) {
        newMaxY = 1
      }
      // else if( !zoom.isActive ) {
      //   newMaxY = formatMaxY(newMaxY, newMinY === Number.MIN_VALUE ? 0 : newMinY)
      // }
      // TODO
      // if( zoom.isMorphing ) {
      //   console.log('!', formatMaxY(newMaxY, newMinY === Number.MIN_VALUE ? 0 : newMinY))
      // }
    }

    var changed = false

    if (chart.minY.toValue !== newMinY) {
      playAnimation(chart.minY, newMinY, time)
      changed = true
    }

    if (chart.maxY.toValue !== newMaxY) {
      playAnimation(chart.maxY, newMaxY, time)
      changed = true
    }

    if (changed && chart === mainChart.current) {
      updateTextsY(data.current.chartType, textY.old, textY.new, time, mainChart.current.minY, mainChart.current.maxY, textCount.y)
    }
  }

  function onChangeY(chart) {
    chart.rangeY = chart.maxY.value - chart.minY.value

    previewChart.scaleY = -(previewChart.height - previewUi.height * 2) / previewChart.rangeY
    previewChart.offsetY = height - previewChart.minY.value * previewChart.scaleY - previewUi.height

    mainChart.current.scaleY = -(mainChart.current.height - mainChartPaddingTop) / mainChart.current.rangeY
    mainChart.current.offsetY = mainChart.current.height - mainChart.current.minY.value * mainChart.current.scaleY

    needRedraw.main = needRedraw.preview = true
  }

  function updateChart(chart) {
    var xChanged = false
    var yChanged = false

    if (updateAnimation(chart.minX, time, isAnimationEnabled)) xChanged = true
    if (updateAnimation(chart.maxX, time, isAnimationEnabled)) xChanged = true
    if (updateAnimation(chart.minY, time, isAnimationEnabled)) yChanged = true
    if (updateAnimation(chart.maxY, time, isAnimationEnabled)) yChanged = true

    if (xChanged) onChangeX(chart)
    if (yChanged) onChangeY(chart)
  }

  function getTimestamp() {
    return performance.now()
  }

  function onResize(force = false) {
    onResizeCheckedAt = getTimestamp()

    canvasBounds = canvases.chart.getBoundingClientRect()
    var newWidth = canvasBounds.width * PIXEL_RATIO
    var newHeight = canvasBounds.height * PIXEL_RATIO

    var isHeightChanged = height !== newHeight
    var isWidthChanged = width !== newWidth

    if (isWidthChanged || isHeightChanged || force) {

      if( isWidthChanged || force ) {
        width = newWidth
        textCount.x = Math.max(1, Math.floor(width / (textX.width * 2)))
        pieCenter.x = width / 2

        canvases.chart.setAttribute('width', width)
        canvases.previewUi.setAttribute('width', width)

        var newIsFullMonth = width / PIXEL_RATIO > 360
        if( isFullMonth !== newIsFullMonth ) {
          isFullMonth = newIsFullMonth
          updateDatesTitles()
        }
      }

      if( isHeightChanged || force ) {
        height = newHeight
        mainChart.current.height = height - previewChart.height - previewMarginTop

        textCount.y = Math.max(1, Math.floor(mainChart.current.height / textY.height))
        pieCenter.y = mainChartPaddingTop + (mainChart.current.height - mainChartPaddingTop) / 2

        canvases.chart.setAttribute('height', height)
        canvases.previewUi.setAttribute('height', previewChart.height)
      }

      onChangeX(mainChart.current)
      onChangeX(previewChart)
      updateMinMaxY(mainChart.current)
      updateMinMaxY(previewChart)

      needRedraw.main = needRedraw.preview = needRedraw.previewUi = true
    }
  }


  function isZoomFinished() {
    return zoom.animation.value === 0 || zoom.animation.value === 1
  }

  function setIsAnimationEnabled() {
    isAnimationEnabled = data.current.columns.filter(function(_col) { return _col.checked }).length < 11
  }

  function showPreview() {
    if( previewChart.isVisible ) { return }

    previewChart.isVisible = true
    canvasesContainer.style.marginBottom = ''
    canvases.previewUi.classList.remove(CLASS_PREFIX + 'hidden-with-display')
  }

  function hidePreview() {
    if( !previewChart.isVisible ) { return }

    previewChart.isVisible = false
    canvasesContainer.style.marginBottom = (-previewChart.height - 1 * PIXEL_RATIO) / PIXEL_RATIO + 'px'
    canvases.previewUi.classList.add(CLASS_PREFIX + 'hidden-with-display')
  }

  function filterCheckedColumns(_col) {
    return _col.checked
  }

  function render(frameStartAt) {
    if (isDestroyed) return
    time = frameStartAt

    // NOTE: CHECK THAT ALREADY RENDERED IN THIS FRAME
    if (
      data.current !== null &&
      (!lastRenderAt || lastRenderAt < frameStartAt ) &&
      width > 0 &&
      height > 0
    ) {
      if( userHasScrolled || !onResizeCheckedAt || isWindowResized || frameStartAt - onResizeCheckedAt > 500 ) {
        onResize()
        userHasScrolled = false
        isWindowResized = false
      }

      if (updateAnimation(zoom.animation, time, isAnimationEnabled)) {
        if (data.current.chartType === CHART_KINDS.percentage && zoom.animation.value === 1) {
          data.current.chartType = CHART_KINDS.pie
        }

        if( zoom.isActive && isZoomFinished() ) {
          zoom.isZoomingIn = false
          mouse.x = 0
          mouse.y = 0
        }

        needRedraw.main = needRedraw.preview = needRedraw.previewUi = true
      }

      if( data.current.chartType === CHART_KINDS.pie ) {
        for (var i = 0; i < data.current.columns.length; i++) {
          var yColumn = data.current.columns[i]
          if( yColumn.total ) {
            if( updateAnimation(yColumn.total, time, isAnimationEnabled) ) needRedraw.main = true
          }
        }
      }

      if( zoom.animation.halfWayTo != null && zoom.animation.value === ZOOM_IN_ANIMATION_HALF_WAY ) {
        zoom.animation.duration = DURATIONS.zoom - zoom.animation.duration
        playAnimation(zoom.animation, zoom.animation.halfWayTo, time)
        zoom.animation.halfWayTo = null
      }

      if (!zoom.isActive && zoom.animation.value === 0) {
        zoom.isMorphing = false
      }

      if (updateAnimation(pieSelectColumnDelta.current, time, isAnimationEnabled)) needRedraw.main = true
      if (updateAnimation(pieSelectColumnDelta.prev, time, isAnimationEnabled)) needRedraw.main = true

      if (updateAnimation(textX.old.alpha, time, isAnimationEnabled)) needRedraw.main = true
      if (updateAnimation(textX.new.alpha, time, isAnimationEnabled)) needRedraw.main = true
      if (updateAnimation(textY.old.alpha, time, isAnimationEnabled)) needRedraw.main = true
      if (updateAnimation(textY.new.alpha, time, isAnimationEnabled)) needRedraw.main = true

      for (var i = 0; i < data.current.columns.length; i++) {
        var yColumn = data.current.columns[i]
        if (updateAnimation(yColumn.alpha, time, isAnimationEnabled)) needRedraw.main = true
        if (updateAnimation(yColumn.previewAlpha, time, isAnimationEnabled)) needRedraw.preview = true
      }

      updateChart(mainChart.current)
      updateChart(previewChart)

      // mouse

      if (mouse.mode === DRAG_MODES.beginOfTrimmer) {

        var x = mouse.x
        if (x > previewUi.max - mouseArea * 2) x = previewUi.max - mouseArea * 2
        var newMinX = previewChart.fromScreenX(x)
        if (newMinX < previewChart.minX.value) newMinX = previewChart.minX.value

        if( data.current.chartType === CHART_KINDS.pie ) {
          // TODO: CALC ALL THIS IN setMinMaxX ON ZOOM IN INTO PIE
          if( newMinX < mainChart.current.minX.toValue - DAY / 2 ) {
            newMinX = mainChart.current.minX.toValue - DAY
            setMinMaxX(mainChart.current, newMinX, null, DURATIONS.pieMovePreviewTrimmer)
          } else if ( mainChart.current.maxX.toValue - mainChart.current.minX.toValue > DAY && newMinX > mainChart.current.minX.toValue + DAY / 2 ) {
            newMinX = mainChart.current.minX.toValue + DAY
            setMinMaxX(mainChart.current, newMinX, null, DURATIONS.pieMovePreviewTrimmer)
          }
        } else {
          setMinMaxX(mainChart.current, newMinX, null)
        }

      } else if (mouse.mode === DRAG_MODES.endOfTrimmer) {

        var x = mouse.x
        if (x < previewUi.min + mouseArea * 2) x = previewUi.min + mouseArea * 2
        var newMaxX = previewChart.fromScreenX(x)
        if (newMaxX > previewChart.maxX.value) newMaxX = previewChart.maxX.value

        if( data.current.chartType === CHART_KINDS.pie ) {
          // TODO: CALC ALL THIS IN setMinMaxX ON ZOOM IN INTO PIE
          if( newMaxX > mainChart.current.maxX.toValue + DAY / 2 ) {
            newMaxX = mainChart.current.maxX.toValue + DAY
            setMinMaxX(mainChart.current, null, newMaxX, DURATIONS.pieMovePreviewTrimmer)
          } else if ( mainChart.current.maxX.toValue - mainChart.current.minX.toValue > DAY && newMaxX < mainChart.current.maxX.toValue - DAY / 2 ) {
            newMaxX = mainChart.current.maxX.toValue - DAY
            setMinMaxX(mainChart.current, null, newMaxX, DURATIONS.pieMovePreviewTrimmer)
          }
        } else {
          setMinMaxX(mainChart.current, null, newMaxX)
        }

      } else if (mouse.mode === DRAG_MODES.centerOfTrimmer) {

        var startX = mouse.x + mouse.startX
        var newMinX = previewChart.fromScreenX(startX)
        if (newMinX < previewChart.minX.value) newMinX = previewChart.minX.value
        if (newMinX > previewChart.maxX.value - mouse.range) newMinX = previewChart.maxX.value - mouse.range

        if( data.current.chartType === CHART_KINDS.pie ) {
          // TODO: CALC ALL THIS IN setMinMaxX ON ZOOM IN INTO PIE
          if( newMinX >= mainChart.current.minX.toValue + DAY / 2 ) {
            newMinX = mainChart.current.minX.toValue + DAY
            if( previewChart.maxX.value - newMinX >= DAY ) {
              setMinMaxX(mainChart.current, newMinX, newMinX + mouse.range, DURATIONS.pieMovePreviewTrimmer)
            }
          } else if( newMinX <= mainChart.current.minX.toValue - DAY / 2 ) {
            newMinX = mainChart.current.minX.toValue - DAY
            if( newMinX >= previewChart.minX.toValue ) {
              setMinMaxX(mainChart.current, newMinX, newMinX + mouse.range, DURATIONS.pieMovePreviewTrimmer)
            }
          }
        } else {
          setMinMaxX(mainChart.current, newMinX, newMinX + mouse.range)
        }
      }

      if( !popup.isTouching && !popup.isScrolled ) {
        var inMain = isMouseInMain()
        if (inMain && (!zoom.isZoomingIn && isZoomFinished())) {
          if (data.current.chartType === CHART_KINDS.pie) {
            selectPie(Math.floor(mouse.x), Math.floor(mouse.y))
          } else {
            select(mainChart.current.fromScreenX(Math.floor(mouse.x)), Math.floor(mouse.y))
          }
        } else {
          selectPie(null, null)
          select(null, null)
        }
      }


      if (data.current.xColumn.max - data.current.xColumn.min > DAY) {
        showPreview()

        if (needRedraw.preview) {
          needRedraw.preview = false
          renderPreview()
          lastRenderAt = getTimestamp()
        }

        if (needRedraw.previewUi) {
          needRedraw.previewUi = false
          renderPreviewUiAndTrimmer()
          lastRenderAt = getTimestamp()
        }
      } else {
        hidePreview()
        needRedraw.preview = false
      }

      if (needRedraw.main) {
        needRedraw.main = false

        var renderMainStartedAt = getTimestamp()
        const ctx = contexts.chart

        ctx.clearRect(0, 0, width, mainChart.current.height + previewMarginTop)
        ctx.save()

        if (!zoom.isMorphing && mainChart.old && data.old && zoom.animation.value > 0 && zoom.animation.value < 1) {
          updateAnimation(mainChart.old.minX, time, isAnimationEnabled)
          updateAnimation(mainChart.old.maxX, time, isAnimationEnabled)
          mainChart.old.rangeX = mainChart.old.maxX.value - mainChart.old.minX.value
          mainChart.old.scaleX = (width - paddingHor * 2) / mainChart.old.rangeX
          mainChart.old.offsetX = -mainChart.old.minX.value * mainChart.old.scaleX + paddingHor

          renderMain(zoom.isActive ? zoom.animation.value : 1 - zoom.animation.value)

          ctx.globalAlpha = zoom.isActive ? 1 - zoom.animation.value : zoom.animation.value

          if (data.old.chartType === CHART_KINDS.stacked || data.old.chartType === CHART_KINDS.bar) {
            renderStacked(ctx, mainChart.old, data.old, width, paddingHor, zoom.isMorphing, previewChart, mouseSelect.i, zoom.minI, zoom.maxI, zoom.animation)
          } else {
            renderMainPath(ctx, mainChart.old, data.old, ctx.globalAlpha, width, paddingHor, zoom)
          }
        } else
          renderMain()

        if (data.current.chartType === CHART_KINDS.percentage && !isZoomFinished()) {
          renderPieTransition()
          renderPie(zoom.animation.value)
        }

        lastRenderAt = getTimestamp()

        isAnimationEnabled = !isRenderedFirstTime || data.current.columns.filter(filterCheckedColumns).length < 11 || (lastRenderAt - time < (isAnimationEnabled ? 25 : 15))
        isRenderedFirstTime = true
      }
    }

    requestAnimationFrame(render)
  }

  function renderPieTransition() {
    var ctx = contexts.chart

    ctx.globalAlpha = zoom.animation.value

    ctx.beginPath()
    ctx.fillStyle = THEMES_COLORS[theme].background
    var cornersHeight = (mainChart.current.height - mainChartPaddingTop)
    var size = Math.min(cornersHeight, width)

    var _width = size + (width - size) * (1 - zoom.animation.value)
    var _height = size + (cornersHeight - size) * (1 - zoom.animation.value)
    var _left = (width - _width) / 2
    var _top = (cornersHeight - _height) / 2
    var cornersRadius = (Math.min(width / 2, cornersHeight / 2) + 35) * zoom.animation.value
    ctx.fillRect(0, mainChartPaddingTop, _left, cornersHeight)
    ctx.fillRect(_left + _width, mainChartPaddingTop, _left, cornersHeight)

    ctx.fillRect(0, mainChartPaddingTop, width, _top)
    ctx.fillRect(0, mainChartPaddingTop + _top + _height, width, _top)

    fillRoundCorners(ctx, _left, mainChartPaddingTop + _top, _left + _width, mainChartPaddingTop + _top + _height, cornersRadius, THEMES_COLORS[theme].background)
  }

  function selectPie(mouseX, mouseY) {
    if (mouseX === null) {
      if( pieSelectColumn.current > -1 ) {
        pieSelectColumn.prev = pieSelectColumn.current
        pieSelectColumn.current = -1
        pieSelectColumnDelta.prev.value = pieAnimationMove
        playAnimation(pieSelectColumnDelta.prev, 0, time)

        popup.hide()
      }
    } else {
      popup.show()
      popup.title.node.container.classList.add(CLASS_PREFIX + 'hidden-with-display')

      var pieSelectAngle = Math.atan2(pieCenter.y - mouseY, pieCenter.x - mouseX)
      pieSelectAngle -= Math.PI
      if (pieSelectAngle < 0) pieSelectAngle = Math.PI * 2 + pieSelectAngle

      var angle = 0
      var newPieSelectColumn = -1
      for (var i = 0; i < data.current.columns.length; i++) {
        var yColumn = data.current.columns[i]
        var percent = yColumn.totalShowed / data.current.columns.total
        var deltaAngle = Math.PI * 2 * percent
        if (!isNaN(pieSelectAngle) && pieSelectAngle > angle && pieSelectAngle < angle + deltaAngle) {
          newPieSelectColumn = i
          break
        }
        angle += deltaAngle
      }

      if (pieSelectColumn.current !== newPieSelectColumn) {
        if( pieSelectColumn.current > -1 ) {
          pieSelectColumn.prev = pieSelectColumn.current
          pieSelectColumnDelta.prev.value = pieAnimationMove
          playAnimation(pieSelectColumnDelta.prev, 0, time)
        }
        pieSelectColumn.current = newPieSelectColumn
        pieSelectColumnDelta.current.value = 0
        playAnimation(pieSelectColumnDelta.current, pieAnimationMove, time)

        for (let i = 0; i < data.current.columns.length; i++) {
          var yColumn = data.current.columns[i]
          if( i === pieSelectColumn.current ) {
            popup.columns[i].classList.remove(CLASS_PREFIX + 'hidden-with-display')
            ;(function IIFE(valueNode, percentNode, totalShowed) {
              setTimeout(function timeoutPieSelect() {
                valueNode.setText(formatNumber(totalShowed, false), time, true)
                percentNode.setText((percent * 100).toFixed(0) + '%', time, true)
              })
            })(popup.values[i], popup.percents[i], yColumn.totalShowed)
          } else {
            popup.columns[i].classList.add(CLASS_PREFIX + 'hidden-with-display')
          }
        }
      }
      var popupMouseX = mouseX / PIXEL_RATIO
      popup.x = popupMouseX
      popup.x -= popup.width / 2
      var popupTextYWidth = paddingHor + 15 * PIXEL_RATIO
      var maxAllowedRightX = width / PIXEL_RATIO - popup.width - paddingHor

      if (popup.x < paddingHor) {
        popup.x = paddingHor
      } else if (popup.x > maxAllowedRightX) {
        popup.x = maxAllowedRightX
      }

      popup.y = (piePopupTopMargin + popupTopMargin) / PIXEL_RATIO

      popup.node.style.transform = `translate(${popup.x}px, ${popup.y}px)`
    }
  }

  function renderPie(globalAlpha = 1) {
    var ctx = contexts.chart

    ctx.globalAlpha = globalAlpha

    var total = 0
    for (var i = 0; i < data.current.columns.length; i++) {
      var yColumn = data.current.columns[i]
      if( yColumn.total == null ) {
        var colTotal = 0
        for (var j = mainChart.current.minI; j < mainChart.current.maxI; j++) {
          colTotal += yColumn.data[j]
        }
        yColumn.total = createAnimation(colTotal, DURATIONS.pieSliceResize)
      }
      yColumn.totalShowed = yColumn.total.value * yColumn.alpha.value
      total += yColumn.totalShowed
    }
    data.current.columns.total = total

    var size = Math.min(width, mainChart.current.height - mainChartPaddingTop)
    var angle = 0

    for (var i = 0; i < data.current.columns.length; i++) {
      var yColumn = data.current.columns[i]
      if (yColumn.total.value === 0) continue

      var percent = yColumn.totalShowed / data.current.columns.total
      var deltaAngle = Math.PI * 2 * percent
      var textAngle = angle + deltaAngle / 2

      var deltaX = 0
      var deltaY = 0

      if (i === pieSelectColumn.current) {
        deltaX = Math.cos(textAngle) * pieSelectColumnDelta.current.value
        deltaY = Math.sin(textAngle) * pieSelectColumnDelta.current.value
      } else if (i === pieSelectColumn.prev) {
        deltaX = Math.cos(textAngle) * pieSelectColumnDelta.prev.value
        deltaY = Math.sin(textAngle) * pieSelectColumnDelta.prev.value
      }

      ctx.fillStyle = data.current.colors[yColumn.name]
      ctx.beginPath()
        ctx.moveTo(pieCenter.x + deltaX, pieCenter.y + deltaY)
        ctx.arc(pieCenter.x + deltaX, pieCenter.y + deltaY, size / 2, angle, angle + deltaAngle, false)
        ctx.lineTo(pieCenter.x + deltaX, pieCenter.y + deltaY)
      ctx.fill()
      angle += deltaAngle

      var fontSize = Math.min(20, deltaAngle * 180 / Math.PI)
      ctx.font = `bold ${fontSize * PIXEL_RATIO}px Arial`
      ctx.fillStyle = WHITE_COLOR
      ctx.fillText((percent * 100).toFixed(0) + '%', pieCenter.x + deltaX + (size * 0.33) * Math.cos(textAngle) - (fontSize / 2) * PIXEL_RATIO, pieCenter.y + deltaY + (size * 0.33) * Math.sin(textAngle) + (fontSize / 5) * PIXEL_RATIO)
    }

    ctx.font = DEFAULT_FONT
  }

  function renderPreview() {
    var ctx = contexts.chart

    ctx.clearRect(0, height - previewChart.height - 1, width, previewChart.height + 1)
    ctx.save()

    ctx.beginPath()
    ctx.rect(paddingHor, height - previewChart.height + previewUi.height, width - paddingHor * 2 - 1, previewChart.height - previewUi.height * 2)
    ctx.clip()

    renderPreviewChart()

    ctx.restore()
  }

  function renderPreviewChart() {
    var ctx = contexts.chart

    ctx.globalAlpha = 1

    if (data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) {
      renderStacked(ctx, previewChart, data.current, width, paddingHor, zoom.isMorphing, previewChart, mouseSelect.i, zoom.minI, zoom.maxI, zoom.animation)
      return
    }

    if (data.current.chartType === CHART_KINDS.percentage || data.current.chartType === CHART_KINDS.pie) {
      renderPercentage(ctx, previewChart, height, previewUi.height, data.current, width, paddingHor)
      return
    }

    for (var i = 0; i < data.current.columns.length; i++) {
      var yColumn = data.current.columns[i]

      if (yColumn.previewAlpha.value === 0) continue

      var columnScaleY = previewChart.scaleY
      var columnOffsetY = previewChart.offsetY

      if (yColumn.alpha.toValue === 0) {
        columnScaleY = yColumn.saveScaleY
        columnOffsetY = yColumn.saveOffsetY
      } else {
        var columnRangeY = yColumn.maxY - yColumn.minY
        if (columnRangeY > previewChart.rangeY) {
          columnScaleY = -(previewChart.height - 2 * previewUi.height) / columnRangeY
          columnOffsetY = height - previewChart.minY.value * columnScaleY
        }
      }

      columnScaleY *= yColumn.scaleY

      ctx.globalAlpha = yColumn.previewAlpha.value
      ctx.lineWidth = previewLineWidth
      renderPath(ctx, width, paddingHor, zoom, yColumn, previewChart.scaleX, columnScaleY, previewChart.offsetX, columnOffsetY, previewChart, data.current)
    }
  }

  function renderPreviewUiAndTrimmer() {
    var ctx = contexts.previewUi

    ctx.clearRect(0, 0, width, previewChart.height)

    ctx.globalAlpha = THEMES_COLORS[theme].previewAlpha
    ctx.beginPath()
    ctx.rect(paddingHor, previewUi.height, previewUi.min - paddingHor + previewUi.radius, previewChart.height - previewUi.height * 2)
    ctx.rect(previewUi.max - previewUi.radius, previewUi.height, width - previewUi.max - paddingHor + previewUi.radius, previewChart.height - previewUi.height * 2)
    ctx.fillStyle = THEMES_COLORS[theme].preview
    ctx.fill()

    ctx.globalAlpha = 1

    if( isThemeLight ) {
      ctx.beginPath()
      ctx.lineCap = 'square'
      ctx.strokeStyle = WHITE_COLOR
      ctx.lineWidth = previewVerticalInsideLineW

      // NOTE: ON LEFT INNER SIDE
      ctx.moveTo(previewUi.min + previewUi.width + previewVerticalInsideLineW / 2, previewChart.height - previewUi.height)
      ctx.lineTo(previewUi.min + previewUi.width + previewVerticalInsideLineW / 2, previewUi.height)

      // NOTE: ON RIGHT INNER SIDE
      ctx.moveTo(previewUi.max - previewUi.width - previewVerticalInsideLineW / 2, previewChart.height - previewUi.height)
      ctx.lineTo(previewUi.max - previewUi.width - previewVerticalInsideLineW / 2, previewUi.height)

      let _x = previewUi.min - 1
      const _y = 0
      let _xRight = _x + previewUi.width

      ctx.moveTo(_xRight, _y)
      ctx.quadraticCurveTo(_x, _y, _x, _y + previewUi.width)
      ctx.lineTo(_x, previewChart.height - previewUi.width)
      ctx.quadraticCurveTo(_x, previewChart.height, _xRight, previewChart.height)

      // NOTE: RIGHT WHITE LINE OUTSIDE

      _x = previewUi.max - previewUi.width
      _xRight = previewUi.max + 1

      ctx.moveTo(_x, _y)
      ctx.quadraticCurveTo(_xRight, _y, _xRight, _y + previewUi.width)
      ctx.lineTo(_xRight, previewChart.height - previewUi.width)
      ctx.quadraticCurveTo(_xRight, previewChart.height, _x, previewChart.height)

      ctx.stroke()
    }

    ctx.beginPath()
    // NOTE: HORIZONTAL TOP...
    ctx.rect(previewUi.min + previewUi.width, 0, previewUi.max - previewUi.min - previewUi.width * 2, previewUi.height)
    // ...& BOTTOM
    ctx.rect(previewUi.min + previewUi.width, previewChart.height - previewUi.height, previewUi.max - previewUi.min - previewUi.width * 2, previewUi.height)
    // NOTE: ROUNDED LEFT & RIGHT
    drawRoundRect(ctx, previewUi.min, 0, previewUi.width, previewChart.height, previewUi.radius, 0, previewUi.radius, 0)
    drawRoundRect(ctx, previewUi.max - previewUi.width, 0, previewUi.width, previewChart.height, 0, previewUi.radius, 0, previewUi.radius)
    ctx.fillStyle = THEMES_COLORS[theme].previewBorder
    ctx.fill()

    ctx.beginPath()
    ctx.lineCap = 'round'
    ctx.strokeStyle = WHITE_COLOR
    ctx.lineWidth = previewVerticalHalfLineW
    ctx.moveTo(previewUi.min + previewUi.width / 2, previewChart.height / 2 - previewLineH / 2)
    ctx.lineTo(previewUi.min + previewUi.width / 2, previewChart.height / 2 + previewLineH / 2)

    ctx.moveTo(previewUi.max - previewUi.width / 2, previewChart.height / 2 - previewLineH / 2)
    ctx.lineTo(previewUi.max - previewUi.width / 2, previewChart.height / 2 + previewLineH / 2)
    ctx.stroke()

    fillRoundCorners(ctx, paddingHor, 0, width - paddingHor, previewChart.height, 10 * PIXEL_RATIO, THEMES_COLORS[theme].background)
  }

  function select(mouseX, mouseY) {
    if (mouseSelect.x !== mouseX) {
      mouseSelect.x = mouseX
      needRedraw.main = true

      if (mouseSelect.x === null) {
        mouseSelect.i = -1
        popup.hide()
      } else {
        popup.show()

        const roundMathFunc = ( data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar ) ? 'floor' : 'round'

        var newSelectI = Math[roundMathFunc]((mouseX - data.current.xColumn.min) / data.current.intervalX) + 1
        if (newSelectI < 1) newSelectI = 1
        if (newSelectI > data.current.xColumn.data.length - 1) newSelectI = data.current.xColumn.data.length - 1

        if (zoom.isMorphing) {
          newSelectI = Math[roundMathFunc]((mouseX - data.current.xColumn.data[zoom.minI]) / (data.current.intervalX / 24)) + 1
          if (newSelectI < 1) newSelectI = 1
          if (newSelectI > data.current.xColumn.toZoomData.length - 1) newSelectI = data.current.xColumn.toZoomData.length - 1
        }

        if (mouseSelect.i !== newSelectI) {
          mouseSelect.i = newSelectI

          var x = zoom.isMorphing ? data.current.xColumn.toZoomData[mouseSelect.i] : data.current.xColumn.data[mouseSelect.i]
          popup.title.node.setText(formatDate(x, formatDateStyles.popup, zoom.isActive), time)

          var total = 0
          for (var i = 0; i < data.current.columns.length; i++) {
            var yColumn = data.current.columns[i]
            var y = zoom.isMorphing ? yColumn.toZoomData[mouseSelect.i] - zoom.offsetY / yColumn.scaleY : yColumn.data[mouseSelect.i]
            if (yColumn.alpha.toValue > 0) total += y
            if( yColumn.alpha.toValue === 0 ) {
              popup.columns[i].classList.add(CLASS_PREFIX + 'hidden-with-display')
            } else {
              popup.columns[i].classList.remove(CLASS_PREFIX + 'hidden-with-display')
            }
            popup.values[i].setText(formatNumber(y, false), time)
          }

          if (data.current.chartType === CHART_KINDS.percentage) {
            for (var i = 0; i < data.current.columns.length; i++) {
              var yColumn = data.current.columns[i]
              var y = zoom.isMorphing ? yColumn.toZoomData[mouseSelect.i] - zoom.offsetY / yColumn.scaleY : yColumn.data[mouseSelect.i]
              popup.percents[i].setText((y * 100 / total).toFixed(0) + '%', time)
            }
          } else if ((data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) && data.current.columns.length > 1) {
            popup.values[popup.values.length - 1].setText(formatNumber(total, false), time)
          }
        }

        var popupMouseX = mainChart.current.toScreenX(mouseX) / PIXEL_RATIO
        popup.x = popupMouseX
        popup.x -= popup.width + leftMarginFromPopup
        if( popup.isScrollable ) {
          popup.x += leftMarginFromPopup * 3
        }
        var maxAllowedLeftX = popupTextYWidth + paddingHor
        if( popup.isScrollable ) {
          maxAllowedLeftX = 0
        }

        var maxAllowedRightX = width / PIXEL_RATIO - popup.width - paddingHor
        if( popup.isScrollable ) {
          maxAllowedRightX += paddingHor
        }

        if( data.current.chartType === CHART_KINDS.yScaled ) {
          if( !popup.isScrollable ) {
            maxAllowedRightX -= popupTextYWidth
          }
        }

        if( popup.x < maxAllowedLeftX ) {
          popup.x = popupMouseX + leftMarginFromPopup
          if( popup.isScrollable ) {
            popup.x -= leftMarginFromPopup * 3
          }
        }

        if (popup.x < maxAllowedLeftX) {
          popup.x = maxAllowedLeftX
        } else if (popup.x > maxAllowedRightX) {
          popup.x = maxAllowedRightX
        }

        popup.node.style.transform = `translate(${popup.x}px, ${popup.y}px)`
      }
    }

    if (mouseSelect.y !== mouseY) {
      popup.node.style.transform = `translate(${popup.x}px, ${popupTopMargin}px)`
    }
  }

  function renderMainChart(chart, globalAlpha) {
    var ctx = contexts.chart

    if (data.current.chartType === CHART_KINDS.stacked || data.current.chartType === CHART_KINDS.bar) {
      renderStacked(ctx, chart, data.current, width, paddingHor, zoom.isMorphing, previewChart, mouseSelect.i, zoom.minI, zoom.maxI, zoom.animation)
    } else if (data.current.chartType === CHART_KINDS.percentage) {
      renderPercentage(ctx, chart, chart.height, mainChartPaddingTop, data.current, width, paddingHor)
    } else {
      renderMainPath(ctx, chart, data.current, globalAlpha, width, paddingHor, zoom)
    }
  }

  function renderChartHorizontalLines() {
    var ctx = contexts.chart

    ctx.strokeStyle = THEMES_COLORS[theme].line
    ctx.lineWidth = lineWidth

    renderLinesY(ctx, textY.old, mainChart.current.toScreenY, mainChart.current.minY, textCount.y, width, paddingHor)
    renderLinesY(ctx, textY.new, mainChart.current.toScreenY, mainChart.current.minY, textCount.y, width, paddingHor)

    ctx.globalAlpha = 1
    ctx.strokeStyle = THEMES_COLORS[theme].line
    ctx.beginPath()
    ctx.moveTo(paddingHor, mainChart.current.height)
    ctx.lineTo(width - paddingHor, mainChart.current.height)
    ctx.stroke()
  }

  function renderMain(globalAlpha = 1) {
    if (data.current.chartType === CHART_KINDS.pie) {
      renderPie()
      return
    }

    var ctx = contexts.chart

    if( data.current.chartType === CHART_KINDS.line || data.current.chartType === CHART_KINDS.yScaled ) {
      ctx.beginPath()
        ctx.rect(0, 0, width, mainChart.current.height + 1)
      ctx.clip()
    }

    ctx.globalAlpha = globalAlpha
    renderMainChart(mainChart.current, globalAlpha)
    renderChartHorizontalLines()
    ctx.restore()


    if (mouseSelect.x) {
      if (data.current.chartType !== CHART_KINDS.stacked) {
        ctx.globalAlpha = 1
        ctx.strokeStyle = THEMES_COLORS[theme].line
        ctx.lineWidth = lineWidth
        ctx.beginPath()
        var x = mainChart.current.toScreenX(mouseSelect.x)
        ctx.moveTo(x, 0)
        ctx.lineTo(x, mainChart.current.height)
        ctx.stroke()
      }

      if (data.current.chartType === CHART_KINDS.line || data.current.chartType === CHART_KINDS.yScaled) {
        var x = zoom.isMorphing ? data.current.xColumn.toZoomData[mouseSelect.i] : data.current.xColumn.data[mouseSelect.i]
        for (var i = 0; i < data.current.columns.length; i++) {
          var yColumn = data.current.columns[i]
          if (yColumn.alpha.toValue === 0) continue
          var y = zoom.isMorphing ? yColumn.toZoomData[mouseSelect.i] : yColumn.data[mouseSelect.i]
          ctx.strokeStyle = data.current.colors[yColumn.name]
          ctx.fillStyle = THEMES_COLORS[theme].background
          ctx.lineWidth = circle.lineWidth
          ctx.beginPath()
          ctx.arc(x * mainChart.current.scaleX + mainChart.current.offsetX, y * mainChart.current.scaleY * yColumn.scaleY + mainChart.current.offsetY, circle.radius, 0, Math.PI * 2)
          ctx.stroke()
          ctx.fill()
        }
      }
    }

    renderTextsXAndY()
  }

  function renderTextsXAndY() {
    var ctx = contexts.chart

    ctx.fillStyle = THEMES_COLORS[theme].text
    ctx.font = DEFAULT_FONT
    renderTextsX(ctx, textX.old, mainChart.current, data.current, textX.width, textCount.x, textX.margin, zoom.isActive)
    renderTextsX(ctx, textX.new, mainChart.current, data.current, textX.width, textCount.x, textX.margin, zoom.isActive)

    renderTextsY(ctx, textY.old, data.current.chartType, textCount.y, mainChart.current.toScreenY, mainChart.current.minY, zoom.offsetY, width, paddingHor, textY.margin, data.current, textX.width)
    renderTextsY(ctx, textY.new, data.current.chartType, textCount.y, mainChart.current.toScreenY, mainChart.current.minY, zoom.offsetY, width, paddingHor, textY.margin, data.current, textX.width)
  }

  function listenThemeChange() {
    if( MutationObserver ) {
      var mutationObserver = new MutationObserver(function(mutationsList) {
        for( var i = 0; i < mutationsList.length; i++ ) {
          if( mutationsList[i].attributeName === 'class' ) {
            checkThemeOnHtmlNode()
          }
        }
      })
      mutationObserver.observe(htmlNode, {
        attributes: true,
        childList: false,
        subtree: false
      })
    } else {
      addEventListener(htmlNode, 'darkmode', checkThemeOnHtmlNode)
    }
  }

  function initEventListeners() {
    addEventListener(document, 'mousedown', onMouseDown)
    addEventListener(document, 'touchstart', onTouchDown)

    addEventListener(document, 'mousemove', onMouseMove)
    addEventListener(document, 'touchmove', onTouchMove)

    addEventListener(document, 'mouseup', onMouseUp)
    addEventListener(document, 'touchend', onMouseUp)
    addEventListener(document, 'touchcancel', onMouseUp)

    addEventListener(popup.node, 'touchstart', onTouchStartPopup)
    addEventListener(popup.node, 'touchmove', onTouchMovePopup)
    addEventListener(popup.node, 'touchend', onTouchEndPopup)

    addEventListener(zoomContainer, 'click', onZoomOut)

    addEventListener(checksContainer, 'mousedown', onMouseDownChecks)
    addEventListener(checksContainer, 'touchstart', onTouchDownChecks)
    addEventListener(checksContainer, 'mouseup', clearLongTapTimeoutId)
    addEventListener(checksContainer, 'touchend', clearLongTapTimeoutId)

    addEventListener(window, 'resize', onWindowResize)
    addEventListener(window, 'scroll', onScroll)

    listenThemeChange()
  }

  function removeEventListeners() {
    removeEventListener(document, 'mousedown', onMouseDown)
    removeEventListener(document, 'touchstart', onTouchDown)

    removeEventListener(document, 'mousemove', onMouseMove)
    removeEventListener(document, 'touchmove', onTouchMove)

    removeEventListener(document, 'mouseup', onMouseUp)
    removeEventListener(document, 'touchend', onMouseUp)
    removeEventListener(document, 'touchcancel', onMouseUp)

    removeEventListener(popup.node, 'touchstart', onTouchStartPopup)
    removeEventListener(popup.node, 'touchmove', onTouchMovePopup)
    removeEventListener(popup.node, 'touchend', onTouchEndPopup)

    removeEventListener(zoomContainer, 'click', onZoomOut)

    removeEventListener(checksContainer, 'mousedown', onMouseDownChecks)
    removeEventListener(checksContainer, 'touchstart', onTouchDownChecks)
    removeEventListener(checksContainer, 'mouseup', clearLongTapTimeoutId)
    removeEventListener(checksContainer, 'touchend', clearLongTapTimeoutId)

    removeEventListener(window, 'resize', onWindowResize)
    removeEventListener(window, 'scroll', onScroll)

    removeEventListener(htmlNode, 'darkmode', checkThemeOnHtmlNode)

    mutationObserver && mutationObserver.disconnect()
  }

  function destroy() {
    if( isDestroyed ) { return }

    isDestroyed = true
    removeAllChilds(container)
    removeEventListeners()
  }

}

window.Graph = {
  render: function render(chartContainer, chartData) {
    const chart = new Chart(chartContainer)
    chart.injectData(chartData)
  }
}
