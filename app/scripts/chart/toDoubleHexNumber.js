export default function toDoubleHexNumber(s) {
  if (s.length === 1) return '0' + s
  return s
}
