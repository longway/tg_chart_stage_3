export default function updateAnimation(anim, time, isAnimationEnabled) {
  if (anim.value === anim.toValue) { return false }
  if (anim.progress === 1) { return false }

  if( !isAnimationEnabled ) {
    anim.value = anim.toValue
    return true
  }

  var progress = (time - anim.startTime) / anim.duration
  if (progress < 0) progress = 0
  if (progress > 1) progress = 1
  anim.progress = progress
  var ease = -progress * (progress - 2)
  anim.value = anim.fromValue + (anim.toValue - anim.fromValue) * ease

  return true
}
