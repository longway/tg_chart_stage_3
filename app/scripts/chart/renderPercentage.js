export default function renderPercentage(
  ctx,
  chart,
  bottom,
  paddingTop,
  data,
  width,
  paddingHor,
) {
  var step = Math.floor((chart.maxI - chart.minI) / (width - paddingHor * 2))
  if (step < 1) step = 1

  var offsetX = chart.offsetX

  var buffer = []

  for (var i = chart.minI; i < chart.maxI; i += step) {
    var sum = 0

    for (var j = 0; j < data.columns.length; j++) {
      var yColumn = data.columns[j]
      sum += yColumn.data[i] * yColumn.alpha.value
    }

    buffer[i] = []
    var from = bottom

    for (var j = 0; j < data.columns.length; j++) {
      var yColumn = data.columns[j]
      var y = (yColumn.data[i] * yColumn.alpha.value / sum) * (chart.height - paddingTop)
      buffer[i][j] = from - y
      from -= y
    }
  }

  for (var j = data.columns.length - 1; j >= 0; j--) {
    var yColumn = data.columns[j]
    if (yColumn.alpha.value === 0) continue
    ctx.beginPath()
    ctx.fillStyle = data.colors[yColumn.name]
    ctx.moveTo(data.xColumn.data[chart.minI] * chart.scaleX + offsetX, bottom)
    for (var i = chart.minI; i < chart.maxI; i += step) {
      var x = data.xColumn.data[i]
      var y = buffer[i][j]
      ctx.lineTo(x * chart.scaleX + offsetX, y)
    }
    ctx.lineTo(data.xColumn.data[i - step] * chart.scaleX + offsetX, bottom)
    ctx.fill()
  }
}
