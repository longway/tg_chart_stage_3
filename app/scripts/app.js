var lightTheme = true
function formatNumberToMinTwoDigits(n) {
  if (n < 10) return '0' + n
  return n
}



window.onChangeTheme = function onChangeTheme(e) {
  e = e || window.event
  var target = e.target || e.srcElement

  lightTheme = !lightTheme
  var htmlNode = document.querySelector('html')
  if (lightTheme) {
    target.textContent = 'Switch to Night Mode'
    htmlNode.classList.remove('dark')
  } else {
    target.textContent = 'Switch to Day Mode'
    htmlNode.classList.add('dark')
  }

  var customEvent = new Event('darkmode')
  htmlNode.dispatchEvent(customEvent)
}

function DataLoader(number) {
  var urlPrefix = 'data/' + number + '/'

  function loadJson(url, zoomOut) {
    return new Promise(function(resolve, reject) {
      var request = new XMLHttpRequest()
      request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
          var d = JSON.parse(request.responseText)
          resolve(d)
        }
      }
      request.open('GET', url, true)
      request.send(null)
    })
  }

  this.x_on_zoom = function x_on_zoom(x) {
    var date = new Date(x)
    return loadJson(urlPrefix + date.getUTCFullYear() + '-' + formatNumberToMinTwoDigits(date.getUTCMonth() + 1) + '/' + formatNumberToMinTwoDigits(date.getUTCDate()) + '.json')
  }

  this.load = function load() {
    return loadJson(urlPrefix + 'overview.json')
  }
}


var chartsContainer = document.querySelector('.charts')
//
for (var i = 1; i < 6; i++) {
// for (var i = 5; i < 6; i++) {
// for (var i = 2; i < 3; i++) {
// for (var i = 1; i < 2; i++) {

// BAR
// for (var i = 4; i < 5; i++) {
// STACKED
// for (var i = 3; i < 4; i++) {
  (function(number) {
    var chartContainer = document.createElement('div')
    chartContainer.classList.add('tgchart')
    chartsContainer.appendChild(chartContainer)

    var dataLoader = new DataLoader(number)
    dataLoader.load()
      .then(function(d) {
        console.log('dataLoader d', d)

        // DATA FILTERS
        // for( var i = 0; i < 25; i++) {
        //   var newColName = 'y' + (d.columns.length - 1 + i)
        //   d.colors[newColName] = d.colors.y1
        //   d.names[newColName] = d.names.y1
        //   d.types[newColName] = d.types.y1
        //   var newColData = d.columns[2].slice(1)
        //   newColData.forEach(function(_y, _i) {
        //     newColData[_i] = _y + (i + 1) * 25
        //   })
        //   d.columns.push(
        //     [newColName].concat( newColData )
        //   )
        // }
        //
        // DAYS
        // var daysToAdd = d.columns[0].slice(1).length * 3
        // for( var i = 0; i < daysToAdd; i++) {
        //   d.columns[0].push(
        //     d.columns[0][d.columns[0].length - 1] + 86400000
        //   )
        //   d.columns[1].push(
        //     d.columns[1][d.columns[1].length - 1] + 10
        //   )
        //   d.columns[2].push(
        //     d.columns[2][d.columns[2].length - 1] + 10
        //   )
        // }


        d.title = d.title || 'Chart #' + number
        d.x_on_zoom = dataLoader.x_on_zoom
        Graph.render(chartContainer, d)


        // var xLen = 1460;
        // var colLen = 50;
        // var colors = ["#108BE3","#3497ED","#4BD964","#64ADED","#2373DB","#E8AF14","#FE3C30","#9ED448","#5FB641","#F5BD25","#F79E39","#E65850"];
        //
        // function generateData(data, xLen, colLen) {
        //     var origColLen = data.columns.length - 1;
        //     var origXLen = data.columns[0].length - 1;
        //     var k = 1;
        //     for (var i = origColLen + 1; i <= colLen; i++) {
        //         data.types['y' + (i - 1)] = data.types['y0'];
        //         data.names['y' + (i - 1)] = 'Column ' + (i);
        //         data.colors['y' + (i - 1)] = colors[i % colors.length];
        //         data.columns[i] = data.columns[(i % origColLen) + 1].slice(0);
        //
        //         k *= 0.9;
        //         data.columns[i][0] = 'y' + (i - 1);
        //         for (var j = 1; j < data.columns[i].length; j++) {
        //             data.columns[i][j] = Math.round(k * data.columns[i][j]);
        //         }
        //     }
        //
        //     for (var i = 0; i <= colLen; i++) {
        //         for (var j = data.columns[i].length; j <= xLen; j++) {
        //             if (i == 0) {
        //                 data.columns[i][j] = data.columns[i][j - 1] + (data.columns[i][2] - data.columns[i][1]);
        //             } else {
        //                 data.columns[i][j] = data.columns[i][(j % origXLen) + 1];
        //             }
        //         }
        //     }
        //
        //     return data;
        // }
        //
        //
        //
        //
        //
        // Graph.render(chartsContainer, generateData(d, xLen, colLen));

      })
  })(i)

}
